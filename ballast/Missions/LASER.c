/* arch-tag: 0e0de6c5-8b15-4b25-8b35-21d785e9076f */
/* FROM MIZ - rebuilt from BENGAL components */
/* NEED TO SET PARAMETERS SET IN InitializeLASER */
/* Nov 2015  */

/* Available stages within this mission: */
#define STAGE_PSBALLAST	 0
#define STAGE_STEPS		 1
#define STAGE_TURBULENCE 2
#define STAGE_PROFILE	 3
#define STAGE_SUBDUCTION 4

#define max(a, b)	((a) > (b) ? (a) : (b))


int next_mode(int nmode, double day_time)
{
    static int icall=0;

    static int oldstage=0;
    static int savemode; /* saved value of mode - except error */
    static int savetimer; /* saved value of Timer.enable */
    static int commskipcount=0;  /* Counts skipped comm modes  (not implemented in LASER) */
    static int goodcomm=1;  /* flag to set if this program set COMM mode (as opposed to the emergency COMM after ERROR)*/
    static int icycle; /* cycle counter for Profile mode */
    static double hometime=0; /* time of last home */


    // backup parameter structures to keep them safe during PS ballast
    static struct down Down0;
    static struct drift Drift0;
    static struct settle Settle0;

    int oldmode, i, istep;
    double x;
    oldmode=nmode;

    newmode = 1; // by default, we expect the mode to change. We'll set newmode=0 explicitly when this isn't the case (rare!)


    /* Check if this is error recovery.
     * Several things can happen:
     * 1. Error triggered COMM, which just returnd. In this case, we'll see (nmode==MODE_COMM && goodcomm!=1)
     * 2. Error handling routine decided to ignore the error and called next_mode(MODE_RESTORE)
     * 3. Error handling routine switch stage/mode, which will look just like a normal mode change.
     * Need to check for the first two cases and restore the old mode:
     */
    if ( (nmode == MODE_COMM && goodcomm != 1) || (nmode == MODE_RESTORE) )
    {
        /* just restore the previous mode */
        nmode=savemode;
        log_event("next_mode: ERROR recovery, mode stays at %d\n",nmode);
        newmode = 0; // explicitly state that this is continuation of an old mode
        return(nmode);
    }


    goodcomm=0; /* reset COMM flag */

    switch (stage){
        case STAGE_PSBALLAST:
            /* --------------------- PS BALLAST STAGE ---------------   */
            if (nmode==MODE_START || stage!=oldstage){
                log_event("STAGE %d: PS BALLAST\n", STAGE_PSBALLAST);
                /* Set  HOME at end of DOWN  */
                set_param_int("down_home",1);

                // Disable timers for PS ballast:
                Timer.enable = 0;
                // Save parameters that may be overridden during PS ballast
                Down0 = Down;
                Drift0 = Drift;
                Settle0 = Settle;

                oldstage=stage;
                icall=0;
            }
            ++icall;
            switch(icall){
                    /*PSBallast is an "old style" stage: instead of keeping its own copies of PSBallast.Down, PSBallast.Settle, PSBallast.Drift, etc.,
                     it sets only a few key parameters (e.g., Down.Pmax = PSBallast.depth1), recycling the rest.
                     This is fine for the PS ballst, since it is typically called first, with all the modes at their defaults.

                     Since all other stages are "new style", we no longer need to backup Down, Drift, and Settle.- AS
                     */
                    /**** Initial dive */
                case 1: nmode=MODE_PROFILE_DOWN;  /* Initial captive dive  - short */
                    Down.Pmax = PSBallast.depth1;
                    break;
                case 2: nmode=MODE_SETTLE;
                    /* set settle target at end of dive -- this is where it settles */
                    //Ballast.Target = PotDensity;   // Perhaps we can set Settle.target directly? (make sure Settle.SetTarget==0) Can actually do it automatically by setting Settle.SetTarget==1!
                    Settle.SetTarget = 1;
                    Settle.timeout = PSBallast.settle1_time;
                    Settle.Ptarget = PSBallast.depth1; // This is NOT what controlls the Settle depth (Ballast.target is)! It is here just in case Nfake is used
                    Ballast.Vset=0;  /* do not set ballast.v0 based on this settle */
                    break;
                case 3: nmode=MODE_DRIFT_SEEK;
                    Drift.timeout_sec = PSBallast.drift1_time;   /* very short test drift mode */
                    Drift.closed_time = Drift.timeout_sec+100.; /* don't open drogue in captive dive */
                    break;
                case 4: nmode=MODE_PROFILE_UP;
                    break;
                case 5: nmode=MODE_COMM;
                    goodcomm=1;
                    log_event("next_mode:COMM after test dive\n");
                    break;

                    /**** Actual ballast - longer settle */
                case 6: nmode=MODE_PROFILE_DOWN;
                    Down.Pmax = PSBallast.depth2;
                    hometime=day_time; // Record this home
                    break;
                case 7:
                    nmode=MODE_SETTLE;
                    //Ballast.Target = PotDensity;   // Perhaps we can set Settle.target directly? (make sure Settle.SetTarget==0) Can actually do it automatically by setting Settle.SetTarget==1!
                    Settle.SetTarget = 1;
                    Settle.timeout = PSBallast.settle2_time;
                    Settle.Ptarget = PSBallast.depth2;  // This is NOT what controlls the Settle depth (Ballast.target is)! It is here just in case Nfake is used
                    Settle.seek_time=PSBallast.settle2_seektime;

                    Ballast.Vset = 1;  /* DO set ballast.v0 this time! */
                    break;
                case 8: nmode=MODE_DRIFT_SEEK;
                    Drift.timeout_sec = PSBallast.drift2_time;  /* longer test drift mode */
                    Drift.closed_time = Drift.timeout_sec/5.; //open part way through
                    break;
                case 9: nmode=MODE_PROFILE_UP;
                    // Restore saved parameters:
                    Down = Down0;
                    Drift = Drift0;
                    Settle = Settle0;

                    // enable timers:
                    Timer.enable = 1;
                    stage=STAGE_STEPS;  /* continue as EOS */
                    log_event("End of PS ballast.\n");
                    break;
                default:
                    goto Bad_icall; // will throw ERR_INVALID_ICALL
            }
            break; /* End STAGE_PSBALLAST loop */
        case STAGE_STEPS:
            /* ------------------ EOS aka STEPS STAGE --------------  */
            if (nmode==MODE_START || stage!=oldstage){
                log_event("STAGE %d: EOS\n", STAGE_STEPS);
                oldstage=stage;
                icall=0;
            }
            ++icall;
            switch(icall){
                case 1: nmode=MODE_PROFILE_UP;
                    Up = Steps.Up;
                    break;
                case 2: nmode=MODE_COMM;
                    goodcomm=1;
                    break;
                case 3: nmode=MODE_PROFILE_DOWN;
                    // verify that the down is deep enough
                    for (i=0;i<=3;i++)	{
                        if (Steps.Down.Pmax <= Steps.Settle[i].Ptarget) {
                            log_event("WARNING: down.Pmax (%6.3f) <= Steps.Settle[%d].Ptarget (%6.3f), ", Steps.Down.Pmax,i+1,Steps.Settle[i].Ptarget);
                            Steps.Down.Pmax = Steps.Settle[i].Ptarget+1;
                            log_event("extending to %6.3f\n", Steps.Down.Pmax);
                        }
                    }

                    Down = Steps.Down;
                    Mlb.point=0;  /* initialize recorder */
                    Mlb.record=1;  /* Start recording */
                    if (hometime==0 || day_time-hometime > Home_days){   // Home on this down
                        set_param_int("down_home",  1);
                        hometime=day_time; // remember the time
                    }
                    else {
                        set_param_int("down_home", -1); // do not home
                    }
                    log_event("Start Recording downcast \n");

                    break;
                case 4: nmode=MODE_SETTLE;   /* Settle #1 */
                    Mlb.record=0; // Stop recording
                    log_event("Finish Recording downcast\n");
                    pMlb=&Mlb;
                    // continue, do not break yet!
                case 5:/* Settle #2 */
                case 6:/* Settle #3 */
                case 7:/* Settle #4 */
                    istep = icall-4;
                    Settle = Steps.Settle[istep];
                    /*
                     NB: there's a similar mechanism in ballast.c to set Ballast.Target from density at a given depth (activated with Ballast.SetTarget==8,
                     but it only works for a single depth. This is a more advanced algorithm that remembers the profile (but it needs the profile in the first place!)
                     Eventually, the two should probably be merged. -AS
                     */
                    x=z2sigma(pMlb,Settle.Ptarget);  /* Get new target from saved profile */

                    if (x>0){
                        Settle.Target = x;
                        log_event("Settle Target #%d: %6.3f db %6.3f sigma\n",istep+1,Settle.Ptarget,x-1000);
                    }
                    else {    /* if z2sigma() fails use Ballast.Target from initial ballasting */
                        log_event("ERROR: next_mode, no new target #%d, use %6.3g\n",istep+1,Ballast.Target-1000.);
                    }
                    if (icall==7){
                        icall = 0; // go to the beginning after the last step
                    }
                    break;
                default:
                    goto Bad_icall; // will throw ERR_INVALID_ICALL
            }
            break;/* End STAGE_STEPS loop */

        case STAGE_TURBULENCE:
            /* -----------------   TURBULENCE STAGE ----------------------- */
            if (nmode==MODE_START || stage!=oldstage){
                log_event("STAGE %d: TURBULENCE\n", STAGE_TURBULENCE);
                icall=0;
                oldstage=stage;
            }
            ++icall;
            switch(icall){
                case 1: nmode=MODE_PROFILE_UP;
                    Up = Turb.Up;
                    break;
                case 2: nmode=MODE_COMM;
                    goodcomm=1;
                    break;
                case 3: nmode=MODE_PROFILE_DOWN;
                    Down = Turb.Down;
                    if (hometime==0 || day_time-hometime > Home_days){   // Home on this down
                        set_param_int("down_home",  1);
                        hometime=day_time; // remember the time
                    }
                    else {
                        set_param_int("down_home", -1); // do not home- This is preferred
                    }
                    break;
                case 4: nmode=MODE_PROFILE_UP;  // This is the special Insertion UP
                    Up = Turb.Up3;
                    break;
                case 5: nmode=MODE_DRIFT_ML;
                    // go back to the beginning next
                    icall=0;
                    break;
                default:
                    goto Bad_icall; // will throw ERR_INVALID_ICALL
            }
            break;/* end STAGE_TURBULENCE loop */

        case STAGE_PROFILE:
            /* -----------------   PROFILING STAGE  ----------------------- */
            if (nmode == MODE_START || stage != oldstage) {
                log_event("STAGE %d: PROFILE\n", STAGE_PROFILE);
                icall = 0;
                oldstage = stage;
                icycle = 0; // count the number of cycles
            }
            ++icall;
            switch (icall) {
                STAGE_PROFILE_cases:
                case 1: nmode = MODE_PROFILE_DOWN;
                    Down = Profile.Down;
                    if (hometime == 0 || day_time - hometime > Home_days) {   // Home on this down
                        set_param_int("down_home", 1);
                        hometime = day_time; // remember the time
                    }
                    else {
                        set_param_int("down_home", -1); // do not home - preferred
                    }
                    break;
                case 2: nmode=MODE_PROFILE_UP;
                    Up = Profile.Up;
                    break;
                case 3:
                    if (icycle==Profile.ncycles){ // Do COMM this time
                        nmode=MODE_COMM;
                        goodcomm=1;
                        icycle=0;
                        icall = 0; // then rewind
                    }
                    else {       // Skip COMM this time
                        ++icycle;
                        log_event("Skip COMM this time. Countdown %d\n",icycle);
                        /* rewind now*/
                        icall = 1;
                        goto STAGE_PROFILE_cases;
                    }
                    break;

                default:
                    goto Bad_icall; // will throw ERR_INVALID_ICALL
            }
            break;/* end STAGE_TURBULENCE loop */

        case STAGE_SUBDUCTION:
            /* -----------------   SUBDUCTION STAGE (PLACEHOLDER)----------------------- */
            if (nmode == MODE_START || stage != oldstage) {
                log_event("STAGE %d: SUBDUCTION\n", STAGE_SUBDUCTION);
                icall = 0;
                oldstage = stage;
            }
            ++icall;
            switch (icall) {
                STAGE_SUBDUCTION_cases:
                case 1: nmode=MODE_BOCHA;
                    Bocha = Subduct.Bocha;
                    break;
                case 2:nmode=MODE_COMM;
                    goodcomm=1;
                    break;
                case 3: //decision
                    if (Subduct.dive == 0) {
                        // rewind
                        log_event("Stay on surface\n");
                        icall = 1;
                        goto STAGE_SUBDUCTION_cases;
                    }
                    else {
                        nmode = MODE_PROFILE_DOWN; // Insertion
                        Down = Subduct.Down;
                        set_param_int("down_home", -1); /* No home */
                        log_event("Attempting subduction\n");
                    }
                    break;
                case 4: nmode=MODE_SETTLE;
                    log_event("Settle on Isopycnal: Target: %5.3f Offset by %5.3f\n",
                              PotDensity-1000.,Subduct.Sigoff);
                    Settle=Subduct.Settle;
                    Ballast.Target=PotDensity+Subduct.Sigoff;  // Target is below ML
                    break;
                case 5: nmode=MODE_PROFILE_UP;
                    icall=1;        // Back to COMM
                    break;

                default:
                    goto Bad_icall; // will throw ERR_INVALID_ICALL
            }
            break;/* end STAGE_SUBDUCTION loop */
        default:/* BAD STAGE */
            nmode=MODE_ERROR;
            log_event("ERROR in next_mode(): invalid stage %d\n",stage);
            set_param_int("error_code", ERR_INVALID_STAGE);
    } /* end stage case*/

    if (newmode)
        log_event("Next_mode: %d->%d\n",oldmode,nmode);
    else
        log_event("Next_mode: continue with %d\n", nmode);

    savemode=nmode;
    return(nmode);

    /* Handle the bad icall exception here: */
Bad_icall:
    nmode = MODE_ERROR;
    log_event("ERROR in next_mode(): impossible combination: stage %d, icall %d\n", stage, icall);
    set_param_int("error_code", ERR_INVALID_ICALL);
    return(nmode);
}

/************************************************************/
/*       Timer/Command/Error Exception handling functions	*/
/* Keep this functionality, although it does little in this mission */
/*     MIZ version saved in Ice_Command&Timer.c         */

/*	Each function returns								*/
/*	0 if Ballast.c can proceed normally					*/
/*	-1 if immediate return is needed					*/
/*	1 if the current mode needs to terminate gracefully	*/
/*		(mode is set to new_mode, and next_mode is called prior to return)	*/
/************************************************************/

/* This function will handle timers */
int handle_timer(int mode, double day_time, int timer_command, int *mode_out)
{
    stage=timer_command; /* set new stage */
    log_event("Timer activated. End mode %d New Stage %d\n",mode,stage);
    return (1); /* returning 1 to gracefully end mode */
}

/* This function will handle commands */
int handle_command(int mode, double day_time, int command, int *mode_out)
{
    log_event("Got command %d  Do nothing\n\n",command);
    return(0);
}

#include "SampleLASER.c"
