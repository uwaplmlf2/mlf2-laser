/*
 * arch-tag: MLF2 CONTROL SOFTWARE
 * Time-stamp: <2015-02-21 14:34:50 mike>
 *
 * mlf2_ballast.C Ballasting routine for DLF  - with Matlab interface
 * usage:
 * mlf2_ballast(day,P,T,S,T2,S2,B_in,mode_in,drogue_in,daysec,command,B,mode_out,drogue_out,telem_out)
 *
 * Matlab interface:
 *  [ballast_goal,mode_out,drogue_out,telem_out, Ptable] = ballast(day,P,T,S,T2,S2,B_in,mode_in,drogue_in,daysec,command, Ptable)
 *
 * INPUT
 * day - time in days since start of mission
 * daysec - seconds of the current GMT day
 * P - pressure in
 * db (appropriate for chosen CTD combination)
 * T - temperature in deg. C
 * S- salinity in FU
 * T2, S2 - second (upper) CTD
 * command - integer command word
 * B_in - target bocha position in m^3
 * mode_in - mode at input, one of the MODE* constants in ballast.h
 * drogue_in - drogue position (0=closed,1=open)
 *
 * OUTPUT
 * B - ballaster position target in cubic meters
 * mode_out - mode at output
 * drogue_out - drogue position desired
 * telem_out - output telemetry - to trackpoint in Intrusion floats
 *
 * This routine controls mode transitions
 * and bocha motions for all modes
 * but COMM & ERROR
 * It does not move bocha, but returns B to calling routine.
 * It does not move drogue, but returns drogue_out to calling
 * routine
 *
 * Its actions are controlled by variables described in "ballast.h"
 * It does not specify bocha motions during COMM
 * Callingprogram should not call during COMM
 * nor change the value of "mode" until all communications are
 * finished.
 * Values of Mode outside of valid values will trigger an error
 * and return of mode=MODE_ERROR;
 *
 * 7/14/03 - Start CBLAST03 Mods
 * 11/1/03 - Start ADV float mods
 * 2/1/04 - Additional ADV float mods - add Pycnocline mission
 * 2/27/04 - Add EOS mission
 * 6/8/04 - CBLAST04 Mods
 * 7/11/04 - Add GASTEST mission
 * 7/14/04 - Replace with different GASTEST mission
 * 7/28/04- final CBLAST04 changes, add HURRICANEGAS mission, add Creep
 * 8/21/04 - minor mods to PYCNOCLINE mission for equatorial expendable - variables changed
 * 9/06/04 - screening for zero CTD values added - PYCNOCLINE mission only
 * 1/29/05 - Put missions in separate files.  Use Include to put them in if necessary
 * 2/21/05 - South China mods
 * 4/28/06 - AESOP mods - considerable changes
 * 9/21/06 - DOGEE gas float changes
 * 5/30/07 - Intrusion acoustic command floats - extra output variable telemetry_out
 * 11/11/07 - NAB
 * 6/23/08 - Hurricane 2008 - only mission changes
 * 3/19/009 - Hurricane 2009 - added z2sigma routine
 * 11/10/10 - Lake Washington - Add Depth error band - Drift and settle modes only
 * 3/311      - LATMIX
 * 10/06/11   -Lake Washington 2 - add Settle.secOday timeout
 * 5/21/12   - "Global timer" functionality added (AS)
 * 5/21/12   - second "up" added (AS)
 * 7/20/12  - Ptable support in Matlab added. ballast() now returns Ptable
 * 2014 - flags for '-100' bad T/S, Special GTD sampling code
 * 10/12/2014 - Add SLEEP mode support
 *  2/12/2015 - BOCHA Mode added
 *  3/16/2015 - Dedicated functions to handle Timer, Command, and Error exceptions (software interrupts)
 *  3/30/2015 - Rename BOCHA - > SERVO_P mode
 *  3/30/2015 - New BOCHA (B=const) mode
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ballast.h"    /*  This contains function & structure definitions */
#include "mtype.h"      /* Mission-type macros */

#define max(a, b)       ((a) > (b) ? (a) : (b))

#ifndef SIMULATION
# include <stdarg.h>
# include <unistd.h>
# include "util.h"
# include "ptable.h"
# include "config.h"
# include "log.h"

void ballast_log(const char *fmt, ...);
#else // SIMULATION
/* __________  next section only for Matlab use __________*/
#include "mex.h"
#include "ptable_mex.h"
#define log_event printf
#define ballast_log printf

/* Input Arguments */

#define DAY_IN          prhs[0]
#define P_IN            prhs[1]
#define T_IN            prhs[2]
#define S_IN            prhs[3]
#define T2_IN           prhs[4]
#define S2_IN            prhs[5]
#define B_IN            prhs[6]
#define M_IN            prhs[7]
#define D_IN            prhs[8]
#define DSEC_IN      prhs[9]
#define CMD_IN       prhs[10]
#define PTABLE_IN       prhs[11]

/* Output Arguments */

#define B_OUT   plhs[0]
#define M_OUT   plhs[1]
#define D_OUT   plhs[2]
#define T_OUT   plhs[3]
#define PTABLE_OUT      plhs[4]

/* mode numbers to pass to the simulator via Ptable */
static short _MODE_PROFILE_DOWN = MODE_PROFILE_DOWN;
static short _MODE_SETTLE = MODE_SETTLE;
static short _MODE_PROFILE_UP = MODE_PROFILE_UP;
static short _MODE_DRIFT_ISO = MODE_DRIFT_ISO;
static short _MODE_DRIFT_SEEK = MODE_DRIFT_SEEK;
static short _MODE_DRIFT_ML = MODE_DRIFT_ML;
static short _MODE_PROFILE_SURF = MODE_PROFILE_SURF;
static short _MODE_SERVO_P = MODE_SERVO_P;
static short _MODE_SETTLE_P = MODE_SETTLE_P;
static short _MODE_BOCHA = MODE_BOCHA;
static short _NR_REAL_MODES = NR_REAL_MODES;
static short _MODE_GPS = MODE_GPS;
static short _MODE_COMM = MODE_COMM;
static short _MODE_ERROR = MODE_ERROR;
static short _MODE_DONE = MODE_DONE;
static short _MODE_START = MODE_START;
static short _MODE_SLEEP = MODE_SLEEP;
static short _MODE_XFER = MODE_XFER;
/* emulation of parameters normally defined elsewhere */
static int down_home = 0;
static short error_code = 0;
static short primary_pr = 0;
#endif /* !SIMULATION */


/*__________________________________________________________*/
/* Mode switching Routine  next_mode.c
 * Call this at the end of each mode
 * It determines which mode is next and changes parameters as appropriate
 *
 * Different missions have different versions of this subroutine
 * Code is not here, but put in by include at compilation
 */

/* global variables: */
static double save1,save2,save3,STsave[5],ICsave[4],SDsave[13];
static int SDisave[7],isave;
static short Pgoalset=-1; /* 1 if Ballast.Target has been set */
static short commhome=1;      /* home at end of comm, 1=yes, 2=no */
static double PotDensity;            /* global potential density */
static double PressureG;             /* global Pressure */
short newmode = 1; /* indicates that this is the first call of mlf2_ballast in the current mode (previousely, mode_start=-999 was used for that, but it was not global). It is set whenever the mode is changed (in next_mode, most commonly) */


/*
 * These variables MUST be global as they are shared with the sampling code.
*/
unsigned long Sensors[NR_REAL_MODES] = {0L};
short Si[NR_REAL_MODES] = {0};


/* Declare global mode-specific parameters - Tier I
 * These parameters change in real time, depending on the current stage. E.g. the "Down" can be a part of PS ballast, or preceede a Lagrangian drift.
 * Ballast.c operates *only* with Tier I parameters
 * They will be initialized by the function initialize_mission_parameters(), called from INITFUNC
 * The setup function is mission-specific, so it is included as Initialize<MISSION>.c below
 */

static struct up Up;
static struct upsurf UpSurf;
static struct servo_p Servo_P;
static struct down Down;
static struct drift Drift;
static struct settle Settle;
static struct settle_p Settle_P;
static struct bocha Bocha;

/* These are "state" parameters -- not mode-related, but still Tier I*/
static struct ballast Ballast;
static struct ctd CTD;
static struct mlb Mlb;
struct mlb *pMlb; /* pointer at Mlb */
static struct error Error;
static struct bugs Bugs;
static struct butter ButterLow;
static struct timer Timer;

/* Butterworth filter structures */
/* Each holds both filter coeff and previous values
   so a separate structure is needed for each filter*/

/* Prototypes for holding coefficients of each type*/
static struct butter ButterLow= {  /* prototype LP filter */
    50000.,   /*  Tfilt / sec  - only specify this here */
    0.,0.,0.,0.,0.,0.,0.,0.,0.   /* program fills in these */
};

#define CTDBADFLAG -100   // CTD pump is off

/* all of these are specified from above */
static struct butter ButterHi; /* prototype HP filter */
static struct butter FiltPlow;  /* filters for each variable */
static struct butter FiltPhi;
static struct butter FiltPdev;
static struct butter FiltSiglow;


/* Dummy function.  Do not remove */
void MFUNC
{
}


/* ----------------------- FLOAT MISSIONS  & PARAMETERS ---------------- */
/* Other mission files exist but not listed here */
# if defined BENGAL
# include "Missions/InitializeBengal.c"
# include "Missions/Bengal.c"
#endif
# if defined OMZ
# include "Missions/InitializeOMZ.c"
# include "Missions/OMZ.c"
#endif
# if defined ORCA
# include "Missions/InitializeORCA.c"
# include "Missions/ORCA.c"
#endif
# if defined MIZ
# include "Missions/InitializeMIZ.c"
# include "Missions/MIZ.c"
#endif
# if defined LASER
# include "Missions/InitializeLASER.c"
# include "Missions/LASER.c"
#endif


/*__________________________________________________________*/


/* Float ballasting routine starts here   */

/* This subroutine is the float ballasting subroutine to be
 * used in MLF2
 * It is driven by a Matlab simulation program,
 * or  used in the float directly
 */

/* Start set_ballast function  */
void
mlf2_ballast(double       day_time,
             double       Pressure,
             double       Temperature,
             double       Salinity,
             double       Temperature_2,
             double       Salinity_2,
             double       ballast,
             int          mode,
             int          drogue,
             double       daysec,
             int          command,
             double       *B,
             int          *mode_out,
             int          *drogue_out,
             double   *telem_out)
{
    /* ballasting variables internal to this routine */
    static double mode_start=-999;      /* time this mode started, -999 if not
                                         * started yet */

    static double last_P= -1000.;   /* Pressure  at last call */
    static double last_day_time= -1000.;
    static double last_daysec=-1000.;
    static long  iout=0;   /* call counter - controls diagnostic output */
    static long isettle=0;  /* counts number of settle mode calls */
    static long idrift=0;    /* counts drift mode calls */
    static double last_profile_time= -1000;  /* time since last profile */
    static double vsave[Nav];     /* temporarily holds volume estimates for averaging */
    static double Tsave[5];          /* hold mixed layer T & S estimates for median filtering */
    static double Ssave[5];
    static double Rsave[5];       /* in situ density */
    static double Sigsave[5];     /* potential density */
    static double TemperatureP=0.;
    static double SalinityP=0.;
    static int Nquit=0;    /* Counter for Down mode quit */
    static double surfacetime=0.;   /* time in UP mode since reaching UP.Pend */
    double mode_time;   /* time in this mode */
    static double Dsig0=0.;    /* save previous values of Dsig */
    static double Pstart=0;      /* save initial Pressure in a mode */
    static double Mass;           /* Mass corrected for Creep */
    static short nbadctd=0;    /* number of sequential bad CTD values */
    static double Perrtimeref=0;  /* time reference for pressure band errors */
    static short  sflag=0;     /* flag to end settle as soon as isettle increases enough */
    static double last_comm=0; /* time of last comm - used with MaxQuietDays as safety */
    static short nbadflag=0;      /* counts number of flagged CTDs */
    static short GTDcont=0;  /* 1 if GTD is in continuous mode */
    static double Profile_time=0.; /* time since float moved away from initial position in UP/DOWN */



    short end_this_mode = 0; /* This can be set by Command/Timer exception or normal mode logic. If !=0, then the current mode has to terminate gracefully, and next_mode is called */

    static FILE *fout;            /* ballast log */
    double time_step_sec;  /* sampling time step  computed from data */
    double Dsig=0;
    double rho,x,y,vol,PressureP,r,next_P,Plow,Phi,Pdev,Siglow;
    double Density,Potemp;              /* Precompute these */
    short Binit;
    double seek_time_sec, decay_time_sec;
    int i,j,n,result;

    Pdev = 0.;

    *mode_out = mode; // by default, the mode will stay the same. Timer/Error/Command exception handlers can change it before next_mode is called
    *drogue_out = drogue;

    /* printf("M%d",mode); */
    if(last_P== -1000. || mode==MODE_START){ // Even though MODE_START is sent to next_mode at the beginning of each new stage, it is never returned as next mode (not anymore!). Therefore, mode==MODE_START only when sent to ballast() explicitly, such as at the mission start.
        /* first entrance */
        log_event("Ballasting (re)start\n");
        /* ballast_log headers*/

        ballast_log(
            "day_time,mode,mode_time,Settle.Target,Drift.Target,Ballast.Target,Ballast.V0,Ballast.Vdev,Mass, Bugs.weight,Drift.Air,P,T\n");
        ballast_log("1,1,86400,1,1,1,1e6,1e6,1000,1000,1e6,1,1\n");  /* scale factors from MKS */
        log_event("Ballast log time mark %f\n",day_time);

        Drift.Voff=0;  /* Intially guess that Ballast values are OK */
        Mass=Mass0;
        *B=0;
        *telem_out=0.;
        last_P=Pressure;  last_day_time=day_time; last_daysec=daysec;
        //mode_start=-999;
        newmode = 1;
        last_profile_time=day_time;
        Perrtimeref=0.;

        /* INITIALIZATION CALL */
        *mode_out = next_mode(MODE_START,day_time);
        *drogue_out=drogue;

        memset(vsave, 0, sizeof(vsave));  /* zero out vsave */

        return;
    }   /* end first entrance */

    /* negative salinity check (will turn into S=0, which is still a bad CTD, but does not make density NaN)*/
    if (Salinity < 0)
        Salinity = 0.;
    if (Salinity_2 < 0)
        Salinity_2 = 0.;

    /* correct CTD values */
    Pressure=Pressure + CTD.Poffset;  /* pressure at middle of float */
    PressureG=Pressure;  /* Global Pressure */
    if (Temperature!=0. && Salinity !=0.){
        Salinity=Salinity+CTD.BottomSoffset;
        Temperature=Temperature+CTD.BottomToffset;
    }
    if (Temperature_2!=0. && Salinity_2 !=0.){
        Salinity_2=Salinity_2+CTD.TopSoffset;
        Temperature_2=Temperature_2+CTD.TopToffset;
    }

    /* Choose CTD values for use in ballasting */
    /* Default is bottom */
    if (CTD.which==TOPCTD
        && Salinity_2 !=0 && Temperature_2 !=0.){
        if (Pressure > CTD.Ptopmin ){
            Temperature=Temperature_2;
            Salinity=Salinity_2;
        }
        else{    /* In bubble zone, Top CTD is not good */
            /* use previous good values*/
            Temperature=TemperatureP;
            Salinity=SalinityP;
        }
    }
    else if (CTD.which == MEANCTD
             && Pressure > CTD.Ptopmin
             && Salinity_2 !=0. && Temperature_2 !=0.
             && Salinity !=0.     && Temperature !=0. ){
        Salinity=(Salinity+Salinity_2)/2.;
        Temperature=(Temperature+Temperature_2)/2.;
    }
    else if (CTD.which == MAXCTD && Salinity_2 > Salinity){
        Temperature=Temperature_2;
        Salinity=Salinity_2;
    }
    else if (Salinity==0 && Temperature==0 && Pressure > CTD.Ptopmin
             && Salinity_2 !=0. && Temperature_2 !=0.){
        Temperature=Temperature_2;  /* bottom is bad */
        Salinity=Salinity_2;
    }
    else {  /* Default */
        Temperature=Temperature;
        Salinity=Salinity;
    }

    if (Temperature == 0. && Salinity == 0. && CTD.BadMax>0) {  /* bad ctd value & threshold on those is set*/
        /* This is a subtle point: we don't want to run ballasting with bad CTD, but, at the same time
           we don't want to cripple the modes that could run fine without a CTD, e.g. MODE_UP.
           Presently, CTD failure will make the latest mode "stuck" for CTD.BadMax samples, perhaps longer if ERR_BAD_CTD is not fixed.
        */
        // printf("Z");
        nbadctd=nbadctd+1;
        if (nbadctd>CTD.BadMax) {  /* too many bad CTDs in a row, ERROR*/
            set_param_int("error_code", ERR_BAD_CTD);
            mode=MODE_ERROR;
            log_event("Too many bad CTD\n");
            nbadctd=0;  /* reset in case it happens again */
        }
        else if (mode==MODE_COMM) { /* coming out of COM - go head and switch modes  - WHY IS THIS HERE?  */
        }
        else {/* skip this call and hope that next CTD is OK */
            if(Pressure>bottom_P)
                ballast=max(ballast,0)+deep_control*(Pressure-bottom_P); /* deep control. Note that if ballast<0, it is ignored in DEEP control - so that badly misballasted float doesn't sink too much. */
            *mode_out=mode;
            *drogue_out=drogue;
            *B=ballast;
            return;
        }
    }
    else { /* if not bad, then reset counter */
        nbadctd=0;
    }


    TemperatureP=Temperature;
    SalinityP=Salinity;

    time_step_sec=(day_time-last_day_time)*86400.;
    //if (mode_start== -999){
    if (newmode){
        mode_start=day_time;
        newmode = 0;
    }
    mode_time=day_time-mode_start;
    if (last_daysec>daysec) {
        // day rollover has just occured
        last_daysec = -1; // this will enable timers and other events to happen at 00:00
    }


    /*if(fmod(day_time*24,1.0)<fmod(last_day_time*24,1.0)) what is this ??? */

    /* if (iout % 10000==10) log_event("\ntime_step_sec %5.0f\n",time_step_sec); */

    if (Pressure<0)
        PressureP=0;  /* Positive Definite Pressure */
    else
        PressureP=Pressure;

    /* precompute seawater properties */
    Density=sw_dens(Salinity, Temperature,Pressure);
    PotDensity=sw_pden(Salinity, Temperature,Pressure,0);
    Potemp=sw_ptmp(Salinity,Temperature,Pressure,0.);
    /* *telem_out=PotDensity; */
    *telem_out=Pressure;

    if (RHOMIN>0 && (PotDensity<RHOMIN || Density<RHOMIN)){  /* Avoid divide by zero */
        set_param_int("error_code", ERR_ZERO_RHO);
        mode=MODE_ERROR;
        *mode_out=mode;
        *drogue_out=drogue;
        log_event("DENSITY ERROR1  %f  %f\n",Density,PotDensity);
        return;
    }

    /* Modify sampling based on current information & mode */
    sampling(mode, day_time, daysec,mode_time);



    Mass=Mass0+Creep*day_time;  /* Increase mass by Creep */

    /* set BallastTarget from Ballast.Pgoal */
    if (Ballast.SetTarget==8 &&
        ((Pressure-Ballast.Pgoal)*(last_P-Ballast.Pgoal)<0) ){
        Ballast.Target=PotDensity;
        Pgoalset=1;
        log_event("Set Ballast.Target %6.3f at %5.1fdbar \n",Ballast.Target-1000.,Pressure);
    }

    /* record data */
    if (Mlb.record==1  ){
        if (Mlb.point==Nsave){  /* better than just bailing */
            log_event("WARNING: Mlb array full %d points %4.1f db - add at end\n",Mlb.point,Pressure);
            Mlb.point=Nsave-1;
        }
        Mlb.Psave[Mlb.point]=Pressure;
        Mlb.Sigsave[Mlb.point]=PotDensity;
        Mlb.point=Mlb.point+1;
    }
    
    /******************  check global timers ***********************/

    end_this_mode = check_timers(mode, day_time, daysec, last_daysec, mode_out);

    /******************  check command ***********************/
    if (command > 0)
    {
        log_event("COMMAND: %d\n", command);
        end_this_mode = handle_command(mode, day_time, command, mode_out);
        if (end_this_mode == -1)
        {
            // emergency return... Why would we want to do this?
            log_event("Command requested ballast bail out.\n");
            return;
        }
    }
    

    /******************  Start actions based on mode *********************/
    if( mode == MODE_PROFILE_DOWN){   /* Down leg */
        if (mode_time==0){
            Pstart=Pressure; /* save initial value */
            Profile_time=0;
            // log_event("DOWN Start Pressure %6.1f\n",Pstart);
            log_event("DOWN from P=%.1f to P=%.1f\n",Pstart,Down.Pmax);
            ballast=Down.Bmin;
        }

        drogue=Down.drogue;

        if(Pressure>Down.Pmax
           || PotDensity>Down.Sigmax
           || mode_time>=Down.timeout/86400.
           || end_this_mode){  /* end DOWN*/
            if (Ballast.SetTarget==6){
                Ballast.Target=PotDensity;
                log_event("End DOWN. Target set %6.3f\n",PotDensity-1000);
            }
            end_this_mode = 1; // This will prompt the call to next_mode
        }
        else{  /* stay in DOWN mode - Do speed control */
            if (Down.Brate==0.  // Speed control is off
                || Pressure < Pstart + Down.Pspeed ){ // Must move down by Pspeed to start control
                ballast=Down.Bmin; // Start moving bocha
                Profile_time=mode_time;// Don't start control pressure until control starts
            }
            else {
                /* control speed relative to target ascent rate */
                x=Pstart+Up.PHyst+Down.Speed*(mode_time-Profile_time)*86400.;  /* target pressure */
                // log_event("%5.5f  Target %3.1f  P %3.1f  B%4.0f\n",day_time,x,Pressure,ballast*1e6);
                if(Pressure > x+Up.PHyst ){
                    ballast=ballast+Down.Brate*time_step_sec; /* too slow */
                }
                else if(Pressure< x-Up.PHyst ){
                    ballast=ballast-Down.Brate*time_step_sec; /* too fast */
                }
                if (ballast>Up.Bmax) ballast=Up.Bmax;
                else if (ballast<Down.Bmin)ballast=Down.Bmin;
            }
        }
    }

    /*******************************************************/
    else if (mode==MODE_SETTLE){  /* settle mode */

        if (mode_time==0.) { /* First entrance */
            n=0;
            Perrtimeref=mode_time;
            /* First entrance: SET SETTLE GOAL */
            if (Settle.SetTarget==1){
                Settle.Target=PotDensity;
            }
            else if (Settle.SetTarget==2){
                Settle.Target=Ballast.Target;
            }
            else if (Settle.SetTarget==3){
                Settle.Target=Drift.Target;
            }

            /* First entrance: estimate ballast point from volume and CTD */
            /* Use reference S,Th; present pressure and hull vol */
            /* NOTE - THIS IS PRESENT BALLAST POINT - NOT BALLAST POINT AT TARGET */
            vol=Ballast.V0
              - Drift.Compress*Pressure*Ballast.V0
              + Drift.Thermal_exp*(Temperature-Drift.Tref)*Ballast.V0
              + Drift.Air*10./(10.+ Pressure);
            Settle.B0=(Mass+Drift.Moff+Bugs.weight)/Density-vol+Drift.Voff;

            ballast=Settle.B0;   /* get head start on settle ballast setting */
            log_event(
                "Start Settle P  %4.2f B %4.0f Target %6.4f Set %d Air %5.2f\n",
                Pressure,Settle.B0*1.e6,Settle.Target-1000.,Settle.SetTarget,Drift.Air*1.e6);
            isettle=0;
        } /* end First Entrance */

        /* Check for depth error during Settle mode */
        if (Error.Modes==2 || Error.Modes==3){
            if ( Pressure < Error.Pmax && Pressure>Error.Pmin) { /* OK */
                Perrtimeref=mode_time;  /* reset clock */
            }
            if (mode_time-Perrtimeref > Error.timeout/86400.) { /* Too long outside of band  */
                set_param_int("error_code", ERR_PRESSURE_RANGE);
                mode=MODE_ERROR;
                log_event("ERROR: Float outside of [%4.1f %4.1f] longer than %4.0f sec\n",
                          Error.Pmin,Error.Pmax,Error.timeout);
                Perrtimeref=mode_time;  /* reset in case it happens again */
                *mode_out=mode;
                *drogue_out=drogue;
                return;
            }
        }
        else {
            Perrtimeref=mode_time;  /* reset clock */
        }

        /* Seek Settle.rho only for first part of settle
           Allow rate to decay thereafter */

        Dsig=PotDensity - Settle.Target;

        // note, that if seek_time>1, it is given in seconds. Otherwise (seek_time<=1) it is given as fraction of timeout!
        if (Settle.seek_time >1){
            seek_time_sec = Settle.seek_time;
        }
        else {
            seek_time_sec = Settle.timeout*Settle.seek_time;
        }

        // note, that if decay_time>1, it is given in seconds. Otherwise (decay_time<=1) it is given as fraction of seek_time!
        if (Settle.decay_time >1){
            decay_time_sec = Settle.decay_time;
        }
        else {
            decay_time_sec = seek_time_sec*Settle.decay_time;
        }


        if (mode_time<=seek_time_sec/86400.) {
            Dsig0=Dsig;
        }
        else {
            Dsig0=Dsig*exp(-(mode_time-seek_time_sec/86400)/decay_time_sec*86400.);
        }

        /* Add fake stratification for unstratified situations */
        Dsig0=Dsig0 + (Pressure-Settle.Ptarget)*Settle.Nfake*Settle.Nfake*PotDensity/9.8;

        Settle.B0=Settle.B0
          +Dsig0/Settle.Target*Ballast.V0*time_step_sec/Settle.tau;  /* Seeking */

        if (Settle.B0<Settle.Bmin)Settle.B0=Settle.Bmin;   /* Limit Seeking amplitude */
        if (Settle.B0>Settle.Bmax) Settle.B0=Settle.Bmax;

        ballast = Dsig0/Settle.Target*Ballast.V0*Settle.beta + Settle.B0; /*PseudoComp*/

        if (mode_time<Settle.drogue_time/86400.){
            drogue=1;  }        /* remain open for drogue_time  */
        else {drogue=0; }


        /* get float volume */
        vol=(Mass+Drift.Moff+Bugs.weight)/Density -ballast -
          Drift.Air*10./(10.+PressureP);

        vol=vol / (1.-Drift.Compress*Pressure
                   + Drift.Thermal_exp*(Temperature-Drift.Tref));
        /* printf("C: %7.1f\n",vol*1.e6); */
        if (Settle.nav>Nav){Settle.nav=Nav;} /* prevent over/underflow */
        if (Settle.nav<1){Settle.nav=1;}
        vsave[isettle%Settle.nav]=vol;  /* Save recent volume estimates */
        ++isettle;

        /* END SETTLE */
        if ( daysec >= Settle.secOday && last_daysec < Settle.secOday ){
            sflag=1;  /* crossed time - set flag to end settle soon */
            log_event("%4.2f End Settle soon - secOday\n",day_time);
        }
        if ( (mode_time>Settle.timeout/86400. && isettle>Settle.nav )  /* timeout */
             || ( sflag==1 && isettle>Settle.nav )  /* secOday */
             || Settle.nskip==0
             || Settle.timeout <0.
             || end_this_mode){
            sflag=0;

            if ( fabs(Dsig*Ballast.V0) < Settle.weight_error && isettle>Settle.nav) /* good settle ??*/
            {
                printf("### M=%f+%f+%f, Ro=%f, B=%e A=%e,PP=%f, P=%f, C=%e, TE=%e, T=%f, Tr=%f => V=%e\n",
                       Mass,Drift.Moff,Bugs.weight,Density-1000, ballast,Drift.Air,PressureP,Pressure,Drift.Compress,   Drift.Thermal_exp,Temperature,Drift.Tref,vol);
                printf("### M/rho=%f, v-B=%f, v-Air=%f \n",
                       Mass/Density,Mass/Density-ballast,Mass/Density -ballast - Drift.Air*10./(10.+PressureP));
                printf("### chi*P=%f, Texp=%f\n",
                       Drift.Compress*Pressure, Drift.Thermal_exp*(Temperature-Drift.Tref));

                vol=filtmean(vsave,Settle.nav);  /* average Nav values */
                x=0;  /* Compute Error */
                for (i=0;i<Settle.nav;++i)
                    x=x+pow(vol-vsave[i],2.);
                x=sqrt(x/Settle.nav);  /* Stdev of Volume estimates */
                Ballast.Vdev=x;
                if ( x<Settle.Vol_error) {   /* Settling OK  */
                    rho=PotDensity;
                    log_event("%4.2f Settled P %4.2f  Sig0 %6.4f  dweight %4.1f Vol %7.1f (%5.2f) B %6.2f\n",
                              day_time,Pressure,rho-1000, Dsig*Ballast.V0*1000, vol*1e6,x*1.e6,ballast*1.e6);


                    if(Ballast.Vset==1 || Ballast.Vset==2){
                        Ballast.V0=vol;
                        log_event("Set Ballast.V0 %f\n",Ballast.V0*1e6);
                    }
                    if (Ballast.SetTarget==4){  /* set from end of Settle */
                        Ballast.Target=PotDensity;
                        log_event("Setting Ballast.Target from end of Settle (%6.3f)\n",Ballast.Target-1000.);
                    }
                }
                else {   /* Settle NOT OK - do not use  */
                    log_event("Not Settled: P %4.2f  Sig0 %6.4f  dweight %4.1f Vol %7.1f (%5.2f) B %6.2f\n",
                            Pressure,PotDensity-1000, Dsig*Ballast.V0*1000, vol*1e6,x*1.e6,ballast*1e6);
                }
            }
            else
            {
                log_event("Settle not converged: P %4.2f Dsig %6.3f Dweight %4.1f\n",
                        Pressure,Dsig,Dsig*Ballast.V0*1000.);
            }
            end_this_mode = 1; // This will prompt the call to next_mode

        }
    }
    /*******************************************************/
else if (mode == MODE_SETTLE_P) {/* SETTLE_P mode -------  EXPERIMENTAL AT BEST */
		static double dp, dpi;
		static double B0; // estimated local equilibrium ballast
		static double BC; // ballast control offset, requested ballast is B0+BC
		if (mode_time == 0.){
			Pstart = Pressure; /* save initial value */
			log_event("Settle_P from P=%5.1f to %5.1f\n", Pstart, Settle_P.Ptarget);
			drogue = Settle_P.drogue;
			dpi = 0;
			BC = 0;

			switch (Settle_P.SetB0){
			case 1:
				B0 = ballast;
				log_event("Chose B0=B=%5.1f\n", B0*1.e6);
				break;
			case 2:
				// estimate the starting ballast from the density profile
					/* First entrance: estimate ballast point from volume and CTD */
					/* Use reference S,Th; present pressure and hull vol */
					vol = Ballast.V0
						- Drift.Compress*Settle_P.Ptarget*Ballast.V0
					//	+ Drift.Thermal_exp*(Temperature - Drift.Tref)*Ballast.V0
						+ Drift.Air*10. / (10. + Settle_P.Ptarget);
					B0 = (Mass + Drift.Moff + Bugs.weight) / Ballast.Target - vol + Drift.Voff;
					log_event("Chose B0=%5.1f (@Ballast.Target=%5.1f)\n", B0*1.e6, Ballast.Target-1000.);
					break;
			default:
				B0 = Settle_P.B0;
				break;
			}

		}

			dp = Pressure - Settle_P.Ptarget; // Pressure error
			dpi += dp*time_step_sec; // Integral pressure error

			/* forget about the EOS for now...
			// Estimate current ballasting offset, BC
			// It is no the same as B0 calculated at the previous step for 2 reasons:
			// 1. Bocha may not have had time to reach the requested B0. If we try to drive it further than B can move, we'd have to drive it back ("integral windup")
			// 2. We are in a different enviroment

			vol = Ballast.V0
				- Drift.Compress*Pressure*Ballast.V0
				+ Drift.Thermal_exp*(Temperature - Drift.Tref)*Ballast.V0
				+ Drift.Air*10. / (10. + Pressure);
			B0 = (Mass + Drift.Moff + Bugs.weight) / Density - vol + Drift.Voff; // estimated local equilibrium ballasting
			BC = ballast - B0; // current ballast offset

			printf("B0:%f BC:%f ", B0*1e6, BC*1e6);
	//		if (abs(dp)>Settle_P.Pcontrol)		dpi = 0;// outside controllaility band, reset integral error:

			 BC = BC + Settle_P.Kp*dp*time_step_sec + Settle_P.Ki*dpi*time_step_sec;
			// BC = Settle_P.Kp*dp + Settle_P.Ki*dpi;
			 printf("BC':%f\n", BC*1e6);



			 */

			if (Pressure < Settle_P.Pmin)
			{ // drive down from upper limit
				BC = - Settle_P.HardBrate*time_step_sec;
			}
			else if (Pressure > Settle_P.Pmax)
			{ // drive up from lower limit
				BC =   Settle_P.HardBrate*time_step_sec;
			}
			else
			{ // normal control

				// just use dumb closed-loop on dB vs. (P-Po)
				BC = Settle_P.Kp*dp*time_step_sec + Settle_P.Ki*dpi*time_step_sec;

			}
			ballast = ballast + BC;


		//	printf("dpi:%f BC:%f\n",)





		if (    /* end SETTLE_P*/
			mode_time >= Settle_P.timeout / 86400.
	            || end_this_mode)
        {
            log_event("End Settle_P time %4.2f %6.1f dbar %6.0f sec \n", day_time, Pressure, mode_time * 86400);
            end_this_mode = 1; // This will prompt the call to next_mode
        }
    }
    /*******************************************************/
    else if (mode==MODE_PROFILE_UP) {/* Up leg */
        if (mode_time==0.){
            Pstart=Pressure; /* save initial value */
            Profile_time=0;
            ballast=Up.Bmax; // Move Bocha
                        log_event("UP from P=%.1f to P=%.1f\n",Pstart,Up.Pend);
            surfacetime=0.;  /* reset surface clock */
        }

        if (Pressure<Up.Pend && surfacetime==0.){   /* start surface clock */
            surfacetime=mode_time;  /* >0 means clock is running */
            /* printf("Start surface %6.1f %6.0f sec\n",Pressure,mode_time*86400.); */
        }

        /* minimize descent if Speedbrake on */
        if(Up.Speedbrake==1 && Pressure>Pstart ){
            ballast=Up.Bmax;
            drogue=1;  /* speedbrake */
        }
        else {
            drogue=Up.drogue;
            if (Up.Brate==0.  // Speed control is off
                  || Pressure > Pstart - Down.Pspeed ){ // Must move up Pspeed to start control
                ballast=Up.Bmax; // Start moving bocha
                Profile_time=mode_time;// Don't start control pressure until control starts
            }
            else {
                /* control speed relative to target ascent rate */
                x=Pstart-Up.PHyst-Up.Speed*(mode_time-Profile_time)*86400.;  /* target pressure */
                //log_event("%5.5f  Target %3.1f  P %3.1f  B%4.0f\n",day_time,x,Pressure,ballast*1e6);
                if(Pressure > x+Up.PHyst ){
                    ballast=ballast+Up.Brate*time_step_sec; /* too slow */
                }
                else if(Pressure < x-Up.PHyst ){
                    ballast=ballast-Up.Brate*time_step_sec; /* too slow */
                }
                if (ballast>Up.Bmax) ballast=Up.Bmax;
                else if (ballast<Down.Bmin)ballast=Down.Bmin;
            }
        }

        if(    /* end UP*/
                ( Up.surfacetime>0. && surfacetime>0.   /* surfacetime logic */
                && mode_time-surfacetime >=Up.surfacetime/86400.)
                || (Up.surfacetime<=0. && Pressure<Up.Pend)  /*if not; just making sure */
                || mode_time >=Up.timeout/86400.
                || end_this_mode
            ){

            log_event("End UP time %4.2f %6.1f dbar %6.0f sec \n", day_time, Pressure, mode_time * 86400);

            if (Ballast.SetTarget==7){
                Ballast.Target=PotDensity;
                log_event("Setting Ballast.Target to PotDensity (%6.3f)\n",PotDensity-1000.);
            }
            end_this_mode = 1; // This will prompt the call to next_mode
        }

    }
    /*******************************************************/
    else if (mode==MODE_PROFILE_SURF) {/* UpSurf leg */
        if (mode_time==0.){
            Pstart=Pressure; /* save initial value */
            //   log_event("UpSurf Start Pressure %6.1f\n",Pstart);
            log_event("UpSurf from P=%.1f to P=%.1f\n",Pstart,UpSurf.Pend);
            surfacetime=0.;  /* reset surface clock */
        }

        if (Pressure<UpSurf.Pend && surfacetime==0.){   /* start surface clock */
            surfacetime=mode_time;  /* >0 means clock is running */
            /* printf("Start surface %6.1f %6.0f sec\n",Pressure,mode_time*86400.); */
        }

        /* minimize descent if Speedbrake on */
        if(UpSurf.Speedbrake==1 && Pressure>Pstart ){
            ballast=UpSurf.Bmax;
            drogue=1;  /* speedbrake */
        }
        else {
            drogue=UpSurf.drogue;
            if (UpSurf.Brate==0.)ballast=UpSurf.Bmax;
            else {
                /* control speed relative to target ascent rate */
                x=Pstart-Up.Speed*mode_time*86400.;  /* target position */
                if(Pressure > x+UpSurf.PHyst ){
                    ballast=ballast+UpSurf.Brate*time_step_sec; /* too slow */
                }
                else if(Pressure< x-UpSurf.PHyst ){
                    ballast=ballast-UpSurf.Brate*time_step_sec; /* too fast */
                }
                if (ballast>UpSurf.Bmax) ballast=UpSurf.Bmax;
                else if (ballast<0.)ballast=0.;
            }
        }

        if(    /* end UpSurf*/
            ( UpSurf.surfacetime>0. && surfacetime>0.   /* surfacetime logic */
              && mode_time-surfacetime >=UpSurf.surfacetime/86400.)
            || (UpSurf.surfacetime<=0. && Pressure<UpSurf.Pend)  /*if not; just making sure */
            || mode_time >=UpSurf.timeout/86400.
            || end_this_mode
            ){
            /* printf("End UP %6.1f %6.0f sec  Dur %6.0f\n",
             * Pressure,mode_time*86400.,(mode_time-surfacetime)*86400.); */

            if (Ballast.SetTarget==7){
                Ballast.Target=PotDensity;
                log_event("Target set %6.3f\n",PotDensity-1000.);
            }
            end_this_mode = 1; // This will prompt the call to next_mode
        }

    }
    /*******************************************************/
    else if (mode==MODE_SERVO_P) {/* SERVO_P mode */
        static double ballast_request;
        static double Bspeed; /* bocha moving speed (applies below Pgoal) */
        static short Pgoal_reached; //
        if (mode_time==0.){
            Pstart=Pressure; /* save initial value */
            log_event("Servo_P from P=%5.1f bounds %5.1f %5.1f \n",Pstart,Servo_P.Pmin, Servo_P.Pmax);
            drogue=Servo_P.drogue;
            Bspeed = Servo_P.Bspeed;
            Pgoal_reached = 0; // Pgoal has not been reached
            ballast_request = ballast;
         }
        if (Pgoal_reached)
        {   // reduce Bspeed if Pgoal has been reached
            Bspeed = Bspeed*(1 - time_step_sec / Servo_P.BspeedEtime);
        }

        if (Pressure > Servo_P.Pgoal)
        { // we are below Pgoal here - keep pushing Bocha out
            ballast_request = ballast_request + Bspeed*time_step_sec;
        }
        else // above Pgoal, push away from surface quickly by reversing Bspeed
        {
            ballast_request = ballast_request - Servo_P.BspeedShallow*time_step_sec;
            printf("v");
            Pgoal_reached = 1;
        }

        ballast = ballast_request; // we have to keep ballast_request separate from ballast, because the latter can accumulate under-driving errors

        


        if(    /* end SERVO_P*/
            Pressure<Servo_P.Pmin || Pressure>Servo_P.Pmax
           || mode_time >=Servo_P.timeout/86400.
           || end_this_mode)
        {
            log_event("End Servo_P time %4.2f %6.1f dbar %6.0f sec \n",day_time,Pressure,mode_time*86400);

            end_this_mode = 1; // This will prompt the call to next_mode
        }
    }
    /*******************************************************/
    else if (mode == MODE_BOCHA) {/* BOCHA mode */
        if (mode_time == 0.){
            Pstart = Pressure; /* save initial value */
            log_event("Fix B=%f for %4.0fs\n", Bocha.B*1e6,Bocha.timeout);
            drogue = Bocha.drogue;
        }

        if (fabs(ballast - Bocha.B) <= Bocha.Btol)
           end_this_mode = 1; // close enough, will end the mode
        else
           ballast = Bocha.B; // fixed bocha - simple as that

        if (    /* end BOCHA */
            mode_time >= Bocha.timeout / 86400.
            || end_this_mode)
        {
            log_event("End Bocha time %4.2f %6.1f dbar %6.0f sec \n", day_time, Pressure, mode_time * 86400);
            end_this_mode = 1; // This will prompt the call to next_mode
        }
    }
    /*******************************************************/
    else if (mode == MODE_DRIFT_ISO || mode  == MODE_DRIFT_ML ||  mode == MODE_DRIFT_SEEK) { /* Drift Modes  */
        if (mode_time>Drift.closed_time/86400.)drogue=1;
        else    drogue=0;

        if(mode_time==0.){  /* First entry */
            log_event("Drift Start: Ballast.V0 %f\n",Ballast.V0*1e6);
            Perrtimeref=mode_time;
            if (Drift.VoffZero==1){
                /* Assume Ballast.V0 is correct and float is neutral*/
                Drift.Voff=0.;
            }

            /* initialize Median filter with current values*/
            if(Drift.median==1){
                for (i=0;i<5;++i){
                    Tsave[i]=Potemp;
                    Ssave[i]=Salinity;
                    Sigsave[i]=PotDensity;
                }
                idrift=0;
            }
        }/* end first entry */

        /* Check for depth error during Drift mode */
        if (Error.Modes==1 || Error.Modes==3){
            if ( Pressure < Error.Pmax && Pressure>Error.Pmin) { /* OK */
                Perrtimeref=mode_time;  /* reset clock */
            }
            if (mode_time-Perrtimeref > Error.timeout/86400.) { /* Too long outside of band  */
                set_param_int("error_code", ERR_PRESSURE_RANGE);
                mode=MODE_ERROR;
                log_event("ERROR: Float outside of [%4.1f %4.1f] longer than %4.0f sec\n",
                          Error.Pmin,Error.Pmax,Error.timeout);
                Perrtimeref=mode_time;  /* reset in case in happens again */
                *mode_out=mode;
                *drogue_out=drogue;
                return;
            }
        }
        else {
            Perrtimeref=mode_time;  /* reset clock */
        }

        /* GET DENSITY FOR BALLASTING */
        if (Drift.median==1){  /* use 5 point median filter */
            /* Update filter values */
            ++idrift;
            j=idrift%5;
            Tsave[j]=Potemp; /* Theta */
            Ssave[j]=Salinity;
            Sigsave[j]=PotDensity;
            for (i=0;i<5;++i){ /* get saved density at current P */
                x=sw_temp(Ssave[i],Tsave[i],Pressure,0.); /* T at this P */
                Rsave[i]=sw_dens(Ssave[i],x,Pressure);
            }
            Ballast.S0=opt_med5(Ssave);
            Ballast.TH0=opt_med5(Tsave);
            Ballast.P0=Pressure;
            Ballast.rho0=opt_med5(Sigsave);
            rho=opt_med5(Rsave); /* filtered rho at ML S,Th, Current P  */
        }
        else{  /* No filter */
            Ballast.S0=Salinity;
            Ballast.TH0=Potemp;
            Ballast.P0=Pressure;
            Ballast.rho0=PotDensity;
            x=sw_temp(Ballast.S0,Ballast.TH0,Pressure,0.); /* temperature */
            rho=sw_dens(Ballast.S0,x,Pressure);/* rho at ML S,Th;  current P*/
        }

        /* Use this density in ballasting equation */
        if (RHOMIN>0 && rho<RHOMIN){  /* Avoid divide by zero */
            set_param_int("error_code", ERR_ZERO_RHO);
            mode=MODE_ERROR;
            *mode_out=mode;
            *drogue_out=drogue;
            log_event("Density ERROR2 %f\n",rho);
            return;
        }

        /* Filtered Pressure and density */
        if (mode_time==0){
            /* Initialize Butterworth filters (in case they have changed) */
            /* note that ButterLow.Tfilt is master variable satellite setable */
            log_event("INITIALIZE FILTERS  Tfilt %4.0f",ButterLow.Tfilt);
            ButterLowCoeff(ButterLow.Tfilt, &ButterLow);
            ButterHiCoeff(ButterLow.Tfilt, &ButterHi);
            log_event("-> %4.0f\n",ButterLow.Tfilt);
            Binit=1;  /* reset at start of each drift */
        }
        else Binit=0;
        /* Run filters */
        /* Plow=Bfilt(&FiltPlow,Pressure,Pressure,Binit,ButterLow);  Low Pass P */
        Phi=Bfilt(&FiltPhi,Pressure,0.,Binit,ButterHi);        /* High Pass P */
        Pdev=Bfilt(&FiltPdev,fabs(Phi),0.,Binit,ButterLow); /* Low Pass P deviations */
        Siglow=Bfilt(&FiltSiglow,Ballast.rho0,Ballast.rho0,Binit,ButterLow); /* Low Pass Potential Density */

        /* CHOOSE DRIFT MODE VARIANT */
        if ( Pressure<Ballast.MLthreshold * Pdev  ||  Pressure < Ballast.MLmin
             ||( Pressure>Ballast.MLtop && Pressure<Ballast.MLbottom )){
            mode=MODE_DRIFT_ML;     /* ML MODE */
        }
        else { /* ISO MODES */
            if(  Pressure > Drift.seek_Pmin   &&  Pressure <Drift.seek_Pmax  &&
                 Pressure > Ballast.SEEKthreshold*Pdev){ /* with seek */
                mode=MODE_DRIFT_SEEK;
            }
            else {    /* without seek */
                mode=MODE_DRIFT_ISO;
            }
        }

//#if 0
//if (iout % 5 ==0)
//    printf("BBB   %6.4f  %5.1f  %5.1f %5.1f  %5.1f %d\n",day_time,Pressure,Plow,Phi,Pdev,mode);
//#endif

/* SET & MODIFY TARGET DENSITY */
        if (mode_time==0) {
            /* SET DRIFT GOAL */
            if (Drift.SetTarget==1){
                Drift.Target=PotDensity;
                log_event("Set Drift.Target to PotDensity (%6.3f)\n",Drift.Target-1000);
            }
            else if (Drift.SetTarget==2){
                Drift.Target=Ballast.Target;
                log_event("Set Drift.Target to Ballast.Target (%6.3f)\n",Drift.Target-1000);
            }
            else if (Drift.SetTarget==3){
                Drift.Target=Settle.Target;
                log_event("Set Drift.Target to Settle.Target (%6.3f)\n",Drift.Target-1000);
            }
        }
/* change when in ML */
        if ( mode==MODE_DRIFT_ML ){
            if(Ballast.MLsigmafilt==1){
                Drift.Target=Siglow;
            }
            else {
                Drift.Target=Ballast.rho0;
            }
        }

/* Pot. Density Anomaly - filtered or not */
        Dsig=(Drift.Target-Ballast.rho0);

/* ISOPYCNAL SEEKING  */
/* Seek isopycnal with timescale Drift.isotime */
        if( mode==MODE_DRIFT_SEEK){
            Drift.Voff=Drift.Voff -Dsig/Drift.iso_time*time_step_sec/rho*Ballast.V0;
            if (Drift.Voff<Drift.Voffmin)Drift.Voff=Drift.Voffmin;
            Dsig0=Dsig;  /* save isopycnal deviation (I'm not sure why) */
        }

/* estimate average pressure in next time interval*/
        if (mode_time>0) next_P=Pressure+(Pressure-last_P)/2;
        else next_P=Pressure;
        if (next_P<0) next_P=0;  /* could be fancier than this */

/* Compute bug Effect & flap drogue */
        if (Bugs.stop_weight>0. && Bugs.start_weight>0.  && fabs(Bugs.start-Bugs.stop)>30.){
            if (  daysec>=Bugs.start && last_daysec<Bugs.start  && Bugs.weight==0 ){
                log_event("Sunset - bug compenstation ON %3.0f g\n",Bugs.start_weight*1000.);
                Bugs.weight=Bugs.start_weight;
            }
            if (Bugs.weight > 0.) {  /* nighttime and active */
                y=(Bugs.stop-Bugs.start);  /* duration of night/ seconds */
                if (y<0.)y=y+86400.;
                if (y<=0.){
                    log_event("ERROR in Bugs start & stop %4.0f %4.0f\n",Bugs.start,Bugs.stop);
                    Bugs.start= -1.e6;
                    y=1e8;  /* Turn bugs off */
                }
                x=daysec-Bugs.start;
                if(x<0)x=x+86400.;
                if ( x<y){  /* continue night */
                    Bugs.weight=Bugs.start_weight+x/y*(Bugs.stop_weight-Bugs.start_weight);
                    if (fmod(x+Bugs.flap_duration,Bugs.flap_interval)<Bugs.flap_duration){
                        drogue=0;  /* close drogue in flap */
                        /*printf("F");*/
                    }
                }
                else{  /* dawn */
                    log_event("Sunrise (or error) - bug compenstation OFF\n");
                    Bugs.weight=0.;
                }
            }
        }

/* BALLAST */
/* Use reference S,Th; present pressure and hull vol */
        vol=Ballast.V0
          - Drift.Compress*Pressure*Ballast.V0
          + Drift.Thermal_exp*(Temperature-Drift.Tref)*Ballast.V0
          + Drift.Air*10./(10.+ next_P);
        ballast=(Mass+Drift.Moff + Bugs.weight)/rho-vol+Drift.Voff+Ballast.Offset;

/* Add Pseudo-compressibility  */
        ballast=ballast -Drift.iso_Gamma*Dsig*Ballast.V0/rho;

/* END DRIFT  */
        if( ( (Drift.timetype==1) /* time since end of last drift */
                     && day_time-last_profile_time>Drift.time_end_sec/86400.)
            || ( (Drift.timetype==2) /* fraction of day in seconds 0-86400 */
                     && daysec >= Drift.time_end_sec  && last_daysec < Drift.time_end_sec )
            || mode_time > Drift.timeout_sec/86400   /* duration of drift (always active) */
            || end_this_mode
            ){
            log_event("End drift: duration %5.0f sec Sod %5.0f\n",mode_time*86400,daysec);
            last_profile_time=day_time;
            if (Ballast.SetTarget ==5){ /* set Ballast at end of Drift */
                Ballast.Target=PotDensity;
                log_event("Set Ballast.Target from the end of drift (%6.3f)\n",Ballast.Target-1000.);
            }
            if (Ballast.Vset==2 || Ballast.Vset==3){
                Ballast.V0=(Mass+Drift.Moff+Bugs.weight)/Density -ballast
                  - Drift.Air*10./(10.+Pressure);
                Ballast.V0=Ballast.V0/(1.-Drift.Compress*Pressure
                                       + Drift.Thermal_exp*(Temperature-Drift.Tref));
                Drift.Voff=0;
                log_event("Set BallastV0 from Drift End %f \n",Ballast.V0*1.e6);
            }

            end_this_mode = 1; // This will prompt the call to next_mode
        }
    }/* end drift mode code */
    /*******************************************************/
    else if(mode==MODE_COMM) { /* Return from Comm mode */
        last_comm=day_time; /* reset Comm timer */
        end_this_mode = 1; // This will prompt the call to next_mode
    }
    /*******************************************************/
    else if (mode == MODE_XFER) { /* Return from AUX file transfer mode */
        // just like MODE_COMM
        log_event("AUX files remaining: %d\n", get_param_as_int("aux:files_remaining"));
        end_this_mode = 1; // This will prompt the call to next_mode
    }
    /*******************************************************/
    else if (mode==MODE_DONE){
        log_event("DONE!\n");
        //mode_start= -999;
        newmode = 1; // do we care anymore?
    }
    else if (mode == MODE_SLEEP){ /* Return from Sleep mode */
        log_event("ballast:end SLEEP\n");
        end_this_mode = 1; // This will prompt the call to next_mode
    }
    else if (mode==MODE_ERROR){
        /*  Don't call next mode - just let it do a COMM
            mode=next_mode(mode,day_time);
            mode_start=-999;
        */
    }
    else{
        log_event("Cant Get here - NonExistant Mode %d\n",mode);
        set_param_int("error_code", ERR_INVALID_MODE);
        mode=MODE_ERROR;
        //mode_start= -999;
    }
    /********************* This is the end of mode branching **********************************/
    
    /* Check if we need to switch to a new mode (the old one have already been wrapped up gracefully) */
    if (end_this_mode)
    {
        // Note that by now mode_out may have changed by one of the exception handlers - this is Ok! We need to pass *that* to next_mode
        if (mode != *mode_out)
        { // but make a note...
            log_event("Mode changed in mlf2_ballast %d->%d.\n", mode, *mode_out);
        }
        *mode_out = next_mode(*mode_out, day_time);
        //mode_start= -999;
    }

    if (day_time-last_comm > MaxQuietDays ){
        log_event("ERROR:   More than %3.0f days since last Comm on day %4.1f . Now day %4.1f\n",MaxQuietDays,last_comm,day_time);
        set_param_int("error_code", ERR_COMM_OVERDUE);
        *mode_out = MODE_ERROR;
        //mode_start= -999;
    }

    if (Pressure > error_P){
        log_event("Pressure emergency %3.0f\n",Pressure);
        set_param_int("error_code", ERR_PRESSURE);
        *mode_out = MODE_ERROR;
        //mode_start= -999;
    }
    /* DEEP CONTROL */
    if (Pressure > bottom_P){
        ballast=max(ballast,0)+deep_control*(Pressure-bottom_P); // Note that if ballast<0, it is ignored in DEEP control - so that badly misballasted float doesn't sink too much.
        printf("D");
    }
    /* SHALLOW CONTROL */
    if (Pressure<top_P && (is_settle(mode) || is_drift(mode))){
        ballast=ballast-(top_P-Pressure)*shallow_control;
        printf("S");
    };

    if(iout%NLOG==0){
        log_event(
            "%5.3f P %5.2f Ball %6.1f Mode %d Voff:%7.2f Target %6.3f Sig0 %6.3f \n",
            day_time,Pressure,ballast*1.e6,mode,
            Drift.Voff*1.e6,Drift.Target-1000.,PotDensity-1000.);
    }

    /* write Ballast log information  */
#ifdef SIMULATION
    // if( ( iout %NLOG==1 || mode_start== -999) && day_time< LOGDAYSSIM ){
    if ((iout %NLOG == 1 || newmode) && day_time< LOGDAYSSIM){
#else
        // if(  iout %NLOG==1 || mode_start== -999 ){
    if (iout %NLOG == 1 || newmode){
#endif

            ballast_log("%7.6f,%d,%7.6f,%7.4f,%7.4f,%7.4f,%9.3f,%5.2f,%9.3f,%5.2f,%5.2f,%9.6f,%7.4f\n",
                        day_time,mode,mode_time,Settle.Target-1000.,Drift.Target-1000.,Ballast.Target-1000.,
                        Ballast.V0*1e6,Ballast.Vdev*1e6,Mass*1000., Bugs.weight*1000.,Drift.Air*1e6,Pressure,Temperature);

        }

        /* final assignments and updates before returning to calling program*/
        ++iout;
 //        *mode_out=mode;  // already updated!
        *drogue_out=drogue;
        *B=ballast;
        last_P=Pressure;

        last_day_time=day_time;
        last_daysec=daysec;
    }

/* returns mean of array X with maximum and minimum values removed */
    double filtmean(double *X, int N)
    {
        double      min,max,mean;
        int         i;

        /* log_event("Filtering %d elements at 0x%08lx\n", N, (unsigned long)X);*/

        mean=0.;
        if (N<=2) return X[0];   /* Default return */
        min=1e10;
        max=-1e10;
        for (i=0;i<N;++i){
            /* log_event("X[%d] = %g\n", i, X[i]);*/
            if (X[i]<min)
                min=X[i];
            else if (X[i]>max)
                max=X[i];
            mean=mean+X[i];
        }
        mean=(mean-min-max)/(N-2);
        return mean;
    }
/*%=========================================================================
 */
/* Second order Butterworth filter function */
/*                 this filter struc,  input data, initalize?, protoype filter */
    double Bfilt(struct butter *B, double X, double YI, short init, struct butter P)
    {
        double Y;
        if (init==1){  /* initialize filter */
            /* saved values all initialized to first value */
            B->Xp=X;
            B->Xpp=X;
            B->Yp=YI;    /* output values to YI */
            B->Ypp=YI;
            /* coefficients set to prototype values
             * This allows coefficients to be changed by satellite */
            B->A2=P.A2;   B->A3=P.A3;
            B->B1=P.B1;     B->B2=P.B2;   B->B3=P.B3;
            B->Tfilt=P.Tfilt;
            log_event("COEFF:%5.0f %e %e %e %e %e\n",B->Tfilt,B->A2,B->A3, B->B1, B->B2,B->B3);
        }

        /* Evaluate filter */
        Y=B->B1*X  +   B->B2*B->Xp  +  B->B3*B->Xpp
          - B->A2*B->Yp   -   B->A3*B->Ypp;
        /* save values */
        B->Ypp=B->Yp;
        B->Yp=Y;
        B->Xpp=B->Xp;
        B->Xp=X;
        return Y;
    }
/*------------------------------------------------------------------------------------ */
/* returns density of Mixed Layer Base from profile */
/* profile should be  downcast, ending in long monotonic section */
    double getmlb( struct mlb * X)  /* X is pointer to structure */
    {
        double Sigmlb,Pp,dP,Sigmin,Sigmax;
        short Pindex[Nsave],N,Np;
        int imax, i,j;
        /* Structure X:  *save are input data, shallow to deep
         *bin are computed equally spaced
         */

        printf("getmlb start\n");
        imax=X->point-1;  /* maximum index */
        if (imax>Nsave-1){log_event("ERROR: getmlb Err 1 %d\n",imax);
            return -1;}

        /* make monotonic by removing points, bottom up */
        Pp=X->Psave[imax];
        j=imax;
        Pindex[imax]=imax;
        for (i=imax-1;i>=0;--i){    /* find indices of monotonic points */
            if (X->Psave[i]<Pp){  /* good */
                j--;
                if(j<0 || j>Nsave-1){log_event("ERROR: getmlb Err 2 %d\n",j);
                    return -1;}
                Pindex[j]=i;
                Pp=X->Psave[i];
            }
        }
        for (i=imax;i>=j;--i){     /*delete nonmonotonic data and resave array */
            X->Psave[i]=X->Psave[Pindex[i]];
            X->Sigsave[i]=X->Sigsave[Pindex[i]];
        }
        Sigmin=2000;Sigmax=0;  /* also find min and max */
        for (i=0; i<(imax - Pindex[j]); ++i){
            if (i>Nsave-1){log_event("ERROR: getmlb Err 3 %d\n",i);return -1;}

            X->Psave[i]=X->Psave[i+ Pindex[j]];
            X->Sigsave[i]=X->Sigsave[i+ Pindex[j]];
            /* printf("%d  %f %f\n",i,X->Psave[i],X->Sigsave[i]);*/
            if (X->Sigsave[i] >Sigmax)Sigmax=X->Sigsave[i];
            if (X->Sigsave[i] <Sigmin)Sigmin=X->Sigsave[i];
        }
        imax=i;  /*new length */
        /* printf("min %f max %f\n",Sigmin,Sigmax);*/
        log_event("mlb raw data:%d points %5.1f  to %5.1fdb\n",imax,X->Psave[0],X->Psave[imax]);

        /* now grid onto uniform grid */
        for (i=0;i<Ngrid-1;++i){ /* fill grid */
            X->Pgrid[i]=i*X->dP;
            X->Siggrid[i]=-1.;  /* fill with bad flags */
        }
        j=0; /* data point index */
        for (i=0;i<Ngrid-1;++i){ /* for each grid point */
            /* move data to grid, check for overflow */
            while(X->Psave[j+1] < X->Pgrid[i] && j<imax-1 && j<=Nsave-2){++j;}
            if (j>=imax || j>Nsave-2){break;} /* EOD */
            if ( X->Psave[j] >= X->Pgrid[i] ){continue;} /* no data for this grid */

            if (X->Psave[j] < X->Pgrid[i] &&  X->Psave[j+1] >= X->Pgrid[i]){ /* if data */
                dP=(X->Psave[j+1]-X->Psave[j]);
                if (dP<=0) /* check for divide by zero */
                {log_event("ERROR: getmlb Err 4 %f\n",dP);return -1;}

                X->Siggrid[i]=X->Sigsave[j] +  /* interpolate to grid */
                  (X->Sigsave[j+1]-X->Sigsave[j])/dP*(X->Pgrid[i] - X->Psave[j]);
            }
        }
        /*
         * for (i=0;i<Ngrid-1;++i){
         * printf("%d  %f %f\n",i,X->Pgrid[i],X->Siggrid[i]);
         * }*/
        /* count number of points, must be >Nmin */
        N=0;
        for(j=0;j<Ngrid;++j){
            if(X->Siggrid[j]>0)++N;
        }
        log_event("mlb gridded data:%d points %5.1f to %5.1fdb\n",N,X->Pgrid[0],X->Pgrid[N-1]);
        if (N<X->Nmin){log_event("ERROR: getmlb Err 5 %d\n",N);return -1;}

        /* Apply MLB algorithm */
        if (X->dSig==0){log_event("ERROR: getmlb Err 6 %f\n",X->dSig);return -1;}
        j=0; /* safety index */
        imax=(int)(2*fabs(Sigmax-Sigmin)/X->dSig);  /* max loops = twice estimated */

        Sigmlb=Sigmin - 3*X->dSig;
        N=0;Np=0;
        while( N>=Np && Sigmlb<Sigmax){
            ++j;
            if(j>imax){log_event("ERROR: getmlb Err 7 %d\n",j);return -1.;}
            Sigmlb=Sigmlb + X->dSig;
            Np=N;
            N=0;
            for (i=0; i<Ngrid-1;++i){ /*get number of points */
                if( fabs( X->Siggrid[i]-Sigmlb) < X->dSig
                    && X->Siggrid[i]>0)++N;
            }
        }
        if (Sigmlb>=Sigmax || Sigmlb< Sigmin ){
            log_event("ERROR: getmlb Err 8 %f\n",Sigmlb);return -1;}

        return Sigmlb;
    }

/*------------------------------------------------------------------------------------ */
/* returns potential density at Z from logged profile  */
/*    first data that spans the depth is used  */
/*  if this is not found, returns mean density  */
/*  if error, return -1 */
    double z2sigma( struct mlb * X, double Z)  /* X is pointer to structure */
    {
        double Pp,dP,Sigmean,x1,x0,S1,S0,xmin;
        short Pindex[Nsave],N,Np;
        int imax, i,j;
        /* Structure X:  *save are input data
         */

        log_event("z2sigma seeking %6.3f\n",Z);
        imax=X->point-1;  /* maximum index */
        if (imax>Nsave-1 || imax<2 ){log_event("ERROR: z2sigma: bad length %d\n",imax);
            return -1;}
        Sigmean=0;
        for (i=1; i<=imax ; ++i){
            if (i>Nsave-1){log_event("ERROR: z2sigma: index error %d\n",i);return -1;}
            x0=X->Psave[i-1];  /* two points near i */
            x1=X->Psave[i];
            if(i==1){xmin=x0;}
            S1=X->Sigsave[i];
            S0=X->Sigsave[i-1];
            Sigmean=Sigmean-1000.+S1;
            if ( (x0>Z && Z>=x1) || (x1>Z && Z>=x0)) { /* found a point */
                if (x1==x0){
                    return (S1+S0)/2.;
                }
                else{
                    return S1+(S0-S1)/(x0-x1)*(Z-x1);
                }
            }
        }
        Sigmean=1000.+Sigmean/(imax-1);
        log_event("WARNING: z2sigma cannot find z=%6.3f in [%6.3f %6.3f], returning mean (%6.3f)\n",Z, xmin,x1, Sigmean);
        return(Sigmean);
    }

/*------------------------------------------------------------------------------------ */


#ifdef SIMULATION
/* ___________________________cut here_________________________________*/
/*  MATLAB INTERFACE ROUTINE - IGNORE FOR FLOAT INSTALLATION */
    void mexFunction(
        int nlhs,       mxArray *plhs[],
        int nrhs, const mxArray *prhs[]
        )
    {
        double
          *day,*P,*T,*S,*T2,*S2,*B_in,*mode_in,*drogue_in,*daysec0,*command_in;
        double
          *B,*mode_out,*drogue_out, *telem_out;
        int           i_mode_out,i_drogue_out;

        unsigned int  m,n;

        /* Check for proper number of arguments */

        if(nrhs < 11) {
            mexErrMsgTxt("set_ballast requires at least 11 input arguments.");
        } else if (nlhs <4) {
            mexErrMsgTxt(
                "set_ballast requires 4 or 5 output arguments.");
        }

        /*
         * Create a matrix for the return argument */

        B_OUT =mxCreateDoubleMatrix(1, 1, mxREAL);
        M_OUT = mxCreateDoubleMatrix(1, 1,mxREAL);
        D_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);
        T_OUT = mxCreateDoubleMatrix(1, 1, mxREAL);

        /* Assign pointers to the various parameters */

        B = mxGetPr(B_OUT);
        mode_out =mxGetPr(M_OUT);
        drogue_out = mxGetPr(D_OUT);
        telem_out = mxGetPr(T_OUT);
        day = mxGetPr(DAY_IN);
        P = mxGetPr(P_IN);
        T = mxGetPr(T_IN);
        S = mxGetPr(S_IN);
        T2= mxGetPr(T2_IN);
        S2 = mxGetPr(S2_IN);
        B_in =mxGetPr(B_IN);
        mode_in = mxGetPr(M_IN);
        drogue_in= mxGetPr(D_IN);
        daysec0=mxGetPr(DSEC_IN);
        command_in=mxGetPr(CMD_IN);
        /* Initialize Ptable  - manually, since MEX may not be built with a GCC compiler */
        if (init_param_table()==1) {
            INITFUNC(0);
        }
        /* copy values from Matlab's Ptable */
        if (nrhs > 11){
            matlab2ptable(PTABLE_IN);
        }

        /* Do the actual computations in a subroutine */

        mlf2_ballast(*day,*P,*T,*S,*T2,*S2,*B_in,
                     (int)*mode_in,(int)*drogue_in,*daysec0,(int)*command_in,
                     B,&i_mode_out,&i_drogue_out,telem_out);
        /* call MODE_ERROR handling function, normally done in mlf2_main_loop */
        if (i_mode_out == MODE_ERROR)
        {
            i_mode_out = handle_mode_error(get_param_as_int("error_code"));
        }
        *mode_out = i_mode_out;
        *drogue_out=i_drogue_out;
        if (nlhs>4) {
            // return Ptable as a Matlab cell array
            PTABLE_OUT = ptable2matlab(); // should return mxArray*
        }
        return;
    }
# else // (!SIMULATION)

/*
 * Log ballasting diagnostic data.
 */
    void
      ballast_log(const char *fmt, ...)
    {
        va_list     args;
        static short bal_records = 0, bal_max_records = 1000;
        static short bal_file_index = 0;
        static char bal_filename[16];
        FILE        *ofp;

        va_start(args, fmt);

        if(bal_records > bal_max_records || bal_records == 0)
        {
            bal_file_index++;
            sprintf(bal_filename, "bal%05d.txt", bal_file_index);
            if(fileexists(bal_filename))
                unlink(bal_filename);
            bal_records = 0;
        }

        bal_records++;

        if((ofp = fopen(bal_filename, "a")) != NULL)
        {
            vfprintf(ofp, fmt, args);
            fclose(ofp);
        }
        else
            log_error("mission",
                      "Cannot open ballasting file\n");
        va_end(args);
    }
# endif

/*
 * Parameter table initialization.  See ptable.c for details on how
 * the parameter table works.
 *
 * Note that it runs in Matlab as well now!
 */

/*
 * Initialize the mode-control parameters.
 */
    INITFUNC(init_mode_params)
    {


        // memset(Sensors, 0, sizeof(Sensors)); do not need

        Sensors[MODE_PROFILE_UP] =   PROF_SENSORS;
        Sensors[MODE_PROFILE_SURF] = PROF_SENSORS;
        Sensors[MODE_PROFILE_DOWN] = PROF_SENSORS;
        Sensors[MODE_SETTLE] = SETTLE_SENSORS;
        Sensors[MODE_SETTLE_P] = SETTLE_SENSORS;
        Sensors[MODE_DRIFT_ISO] = DRIFT_SENSORS;
        Sensors[MODE_DRIFT_ML] = DRIFT_SENSORS;
        Sensors[MODE_DRIFT_SEEK] = DRIFT_SENSORS;
        Sensors[MODE_SERVO_P] = DRIFT_SENSORS;
        Sensors[MODE_BOCHA] = DRIFT_SENSORS;

        // memset(Si, 0, sizeof(Si)); do not need
        // Sampling intervals (Si) are now set in initialize_mission_parameters()


        /* Create Ptable links to the mode-specific (Tier I) parameters. The rest are in initialize_mission_parameters*/
        /* These macros expand to add_param("Down.Pmax",	PTYPE_DOUBLE, &Down.Pmax), etc.  */
        /* Use ADD_PTABLE_D for PTYPE_DOUBLE, ADD_PTABLE_L for PTYPE_LONG, and ADD_PTABLE_S for PTYPE_SHORT.*/

        /* intervals and samping */
		/* I'd say the names of the Ptable entries should be "sensors[Up]", "sensors[Down]", etc. -AS*/
        add_param("Up:sensors", PTYPE_LONG, &Sensors[MODE_PROFILE_UP]);
        add_param("UpSurf:sensors", PTYPE_LONG, &Sensors[MODE_PROFILE_SURF]);
        add_param("Down:sensors", PTYPE_LONG, &Sensors[MODE_PROFILE_DOWN]);
        add_param("Driftiso:sensors", PTYPE_LONG, &Sensors[MODE_DRIFT_ISO]);
        add_param("Driftml:sensors", PTYPE_LONG, &Sensors[MODE_DRIFT_ML]);
        add_param("Driftseek:sensors", PTYPE_LONG, &Sensors[MODE_DRIFT_SEEK]);
        add_param("Settle:sensors", PTYPE_LONG, &Sensors[MODE_SETTLE]);
        add_param("Settle_P:sensors", PTYPE_LONG, &Sensors[MODE_SETTLE_P]);
        add_param("Servo_P:sensors", PTYPE_LONG, &Sensors[MODE_SERVO_P]);
        add_param("Bocha:sensors", PTYPE_LONG, &Sensors[MODE_BOCHA]);

        add_param("Up:si", PTYPE_SHORT, &Si[MODE_PROFILE_UP]);
        add_param("UpSurf:si", PTYPE_SHORT, &Si[MODE_PROFILE_SURF]);
        add_param("Down:si", PTYPE_SHORT, &Si[MODE_PROFILE_DOWN]);
        add_param("Driftiso:si", PTYPE_SHORT, &Si[MODE_DRIFT_ISO]);
        add_param("Driftml:si", PTYPE_SHORT, &Si[MODE_DRIFT_ML]);
        add_param("Driftseek:si", PTYPE_SHORT, &Si[MODE_DRIFT_SEEK]);
        add_param("Settle:si", PTYPE_SHORT, &Si[MODE_SETTLE]);
        add_param("Settle_P:si", PTYPE_SHORT, &Si[MODE_SETTLE_P]);
        add_param("Servo_P:si", PTYPE_SHORT, &Si[MODE_SERVO_P]);
        add_param("Bocha:si", PTYPE_SHORT, &Si[MODE_BOCHA]);

        /* misc. ungrouped parameters */
        ADD_PTABLE_D(   Mass0  );
        ADD_PTABLE_D(   Creep  );
        ADD_PTABLE_D(   shallow_control  );
        ADD_PTABLE_D(   deep_control  );
        ADD_PTABLE_D(   top_P  );
        ADD_PTABLE_D(   bottom_P  );
        ADD_PTABLE_D(   error_P  );
        ADD_PTABLE_S(   stage  );

        ADD_PTABLE_D(   telem_Step  );
        ADD_PTABLE_S(   commhome  );
        ADD_PTABLE_D(   Home_days  );
        ADD_PTABLE_D(   MaxQuietDays  );
        ADD_PTABLE_D(   sleepduration  );

        /* Butterworth filter parameter - sets both hi and low */
        ADD_PTABLE_D(   ButterLow.Tfilt  );

        ADD_PTABLE_S(   Error.Modes  );
        ADD_PTABLE_D(   Error.Pmin  );
        ADD_PTABLE_D(   Error.Pmax  );
        ADD_PTABLE_D(   Error.timeout  );

        /* here, we are using macros that add all the structure elements to Ptable at once, see ballast.h*/
        ADD_PTABLE_DOWN(   Down  );
        ADD_PTABLE_UP(   Up  );
        ADD_PTABLE_UPSURF(   UpSurf  );
        ADD_PTABLE_SERVO_P(   Servo_P  );
        ADD_PTABLE_SETTLE(   Settle  );
		ADD_PTABLE_SETTLE_P(	Settle_P	);
        ADD_PTABLE_BOCHA(   Bocha );
        ADD_PTABLE_DRIFT(   Drift  );
        ADD_PTABLE_BALLAST(   Ballast  );

        ADD_PTABLE_S(   CTD.which  );
        ADD_PTABLE_L(   CTD.BadMax  );
        ADD_PTABLE_D(   CTD.Ptopmin  );
        ADD_PTABLE_D(   CTD.Poffset  );
        ADD_PTABLE_D(   CTD.Separation  );
        ADD_PTABLE_D(   CTD.TopSoffset  );
        ADD_PTABLE_D(   CTD.TopToffset  );
        ADD_PTABLE_D(   CTD.BottomSoffset  );
        ADD_PTABLE_D(   CTD.BottomToffset  );

        ADD_PTABLE_S(   Mlb.go  );
        ADD_PTABLE_S(   Mlb.Nmin  );
        ADD_PTABLE_D(   Mlb.dP  );
        ADD_PTABLE_D(   Mlb.dSig  );
        ADD_PTABLE_D(   Mlb.Sigoff  );

        /* Bug parameters */
        ADD_PTABLE_D(   Bugs.start  );
        ADD_PTABLE_D(   Bugs.stop  );
        ADD_PTABLE_D(   Bugs.start_weight  );
        ADD_PTABLE_D(   Bugs.stop_weight  );
        ADD_PTABLE_D(   Bugs.flap_interval  );
        ADD_PTABLE_D(   Bugs.flap_duration  );

        /* global timers */
        ADD_PTABLE_S(   Timer.enable  );
		/* Ideally, we'd want to write it like this
			add_array("Timer.time",PTYPE_DOUBLE, Timer.time, NTIMERS);
			add_array("Timer.command", PTYPE_SHORT, Timer.command, NTIMERS);
			add_array("Timer.nskip", PTYPE_SHORT, Timer.nskip, NTIMERS);
		but it'll require Mike to add a function to hix Ptable library.
		For now...*/
		ADD_PTABLE_D(	Timer.time[0]	);
		ADD_PTABLE_D(	Timer.time[1]	);
		ADD_PTABLE_D(	Timer.time[2]	);
		ADD_PTABLE_D(	Timer.time[3]	);
		ADD_PTABLE_S(	Timer.command[0]);
		ADD_PTABLE_S(	Timer.command[1]);
		ADD_PTABLE_S(	Timer.command[2]);
		ADD_PTABLE_S(	Timer.command[3]);
		ADD_PTABLE_S(	Timer.nskip[0]	);
		ADD_PTABLE_S(	Timer.nskip[1]	);
		ADD_PTABLE_S(	Timer.nskip[2]	);
		ADD_PTABLE_S(	Timer.nskip[3]	);

#ifdef SIMULATION
        // normally, these parameters are set and defined elsewhere, but we need them during the simulation
        ADD_PTABLE_S(down_home);
        ADD_PTABLE_S(error_code);
        ADD_PTABLE_S(primary_pr);
        // these parameters are only here to pass the mode IDs to Matlab
		ADD_PTABLE_S(_MODE_PROFILE_DOWN);
		ADD_PTABLE_S(_MODE_SETTLE);
		ADD_PTABLE_S(_MODE_PROFILE_UP);
		ADD_PTABLE_S(_MODE_DRIFT_ISO);
		ADD_PTABLE_S(_MODE_DRIFT_SEEK);
		ADD_PTABLE_S(_MODE_DRIFT_ML);
		ADD_PTABLE_S(_MODE_PROFILE_SURF);
		ADD_PTABLE_S(_MODE_SERVO_P);
		ADD_PTABLE_S(_MODE_SETTLE_P);
		ADD_PTABLE_S(_MODE_BOCHA);
		ADD_PTABLE_S(_NR_REAL_MODES);
		ADD_PTABLE_S(_MODE_GPS);
		ADD_PTABLE_S(_MODE_COMM);
		ADD_PTABLE_S(_MODE_ERROR);
		ADD_PTABLE_S(_MODE_DONE);
        ADD_PTABLE_S(_MODE_START);
		ADD_PTABLE_S(_MODE_SLEEP);
		ADD_PTABLE_S(_MODE_XFER);
# endif
        /* Mission-specific parameter setup. Also makes Ptable links to Mission-specific parameters */
        initialize_mission_parameters(); // the function is included in Initialize<MISSION>.c

#ifdef SIMULATION
        /* dump Ptable (normally this is done in mission.c, but we'd want this to work in simulator) */
        {
            FILE* f = fopen("param.xml","wt");
            dump_params(f);
            fclose(f);
        }
# endif
            }

/*******************************/
/*     check global timers     */
/*******************************/
int check_timers(int mode, double day_time, double daysec, double last_daysec, int *mode_out)
{
    int result = 0;
	int k;
    if (Timer.enable == 0)
    { // All timers disabled, do nothing
        return 0;
    }

    // timers enabled, let's check them!
    // Timer.timeX needs to occur between last_daysec and daysec for timer X to trigger.
	for (k = 0; k < NTIMERS; k++) {
		if (daysec >= Timer.time[k] && last_daysec < Timer.time[k] && Timer.command[k] >= 0) {
			if (Timer.countdown[k] == 0) {   // Is this a multiday timer?
				log_event("Timer #%d (%5.0fsod) -> Timercommand %d\n",k,daysec,Timer.command[k]);
				result = handle_timer(mode, day_time, Timer.command[k], mode_out);
				Timer.countdown[k] = Timer.nskip[k]; // reset
				break;
			}
			else { /* Yes, multiday time - skip this time */
				log_event("Timer #%d (%5.0fsod) Skip. Countdown %d\n", k,daysec,Timer.countdown[k]);
				Timer.countdown[k] = Timer.countdown[k] - 1;  /* decrement multiday index */
			}
		}
	}
return result;
}
