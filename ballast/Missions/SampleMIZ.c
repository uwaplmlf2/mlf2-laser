/* Sampling subroutine: for OMZ
Set float sampling scheme for this mission
Call on every data loop
May 29, 2012 - surface salinity profiler support
Jan 22, 2013 - Now pass mode_time to sampling(), so that we can run SSAL calibration for a given period
Feb 2014 -  modify for OMZ
Feb 23, 2015 - Ice float (MIZ)
*/



int sampling(int nmode, double day_time, double day_sec, double mode_time)
{
    long next_mode_time;

	if (mode_time == 0){
	}

	// each data cycle, reset Altimeter to OFF
	// We may want to turn them back on, but only until the next sampling interval
	DISABLE_SENSORS(nmode, SENS_ALT);
    Ice.SurfcheckRequested = 0;
	// deal with the altimeter
    // Notice that Ice.act_now needs to disable Ice.DelaySec. At the same time, we cannot always request surfcheck if Ice.act_now>0, since we only want to request it once (Ice.IntervalSec applies to subsequent requests)
	if (Ice.Run)
	{
        if (Ice.PhotosTaken == 0 && PressureG <= Ice.MaxPhotoP && !Ice.NeedPhoto)
        { //
            log_event("MaxPhotoP reached (P=%.1f), NeedPhoto->1\n", PressureG);
            Ice.NeedPhoto = 1;
        }

        if ( ((long)(mode_time*86400.) > Ice.DelaySec || Ice.act_now) && (long)((day_time - Ice.day_time_last)*86400.) > Ice.IntervalSec )
		{
            set_param_int("alt:tsample", Ice.DurationSec); // Altimeter burst duration
			ENABLE_SENSORS(nmode, SENS_ALT);
			Ice.day_time_last = day_time;
            Ice.SurfcheckRequested = 1;

            // Check if this is the last run before timeout (assume we are in Servo_P mode, which is BAD!).
            next_mode_time = (long)(mode_time * 86400.) + Ice.IntervalSec + Si[MODE_SERVO_P];

            if (next_mode_time > Servo_P.timeout){
                log_event("Last altimeter run before timeout (next @ %d>%d s)\n", next_mode_time, (long)Servo_P.timeout);
                Ice.act_now = 1;  // Last run... Request "act_now"
            }

            Ice.NeedPhoto |= Ice.act_now; // always take a phot on a last run (indicated by act_now)

            if (Ice.NeedPhoto)
			{
                Ice.SurfcheckRequested = 2; // Tell the simulator that photo was requested as well!
				set_param_int("aux:photo", 1); // request a photo
                Ice.PhotosTaken++;
				log_event("%5.3f ALT* P %4.2f\n", day_time, PressureG); // "ALT*" means "Altimeter+photo"
                Ice.NeedPhoto = 0;
			}
			else
			{
				set_param_int("aux:photo", 0); // no photos
				log_event("%5.3f ALT P %4.2f\n", day_time, PressureG);
			}

		}
	}

return(0);
}
