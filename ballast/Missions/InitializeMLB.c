/* This file is included in ballast.c */
/* It sets initial values of all control variables, so that the main code
	does not need to be modified when values change */

/* 11/14/06  Equatorial MLB mission - updated to DOGEE structures */

/* Control structures */

static struct up Up = {
    650.e-6,    /*250e-6 * Ball */
    1.,        /* Gas 0.02   target Speed db/s */
    0.,        /* Gas 5.e-8    Brate m^3/sec 150cc/1hr */
    5.,        /* Pend */
    4000.,   	/*(3600) timeout */
    1,		/* Speedbrake 1= ON */
    10.,        /* Hysteresis in speed control / m*/
    0.		/* Drogue 1=open, 0 closed */    
};

static struct down Down = {
    130.,       /*Pmax */
    7000.,      /* timeout */
    2000.,     /* Sigmax  (downleg end) */
    0.,	        /* 50e-6 B0 */
    1.,	/* Gas 0.02  target speed m/s */
    0.,	/* Gas 20e-8 Bocha speed */
    0		/* Drogue  0 = closed */    
};

static struct drift Drift = {
    0,   	/* startset  0 -from last drift, 1 - start of drift neutral */
    1,		/* median: 1 use 5 pt median filter, 0 don't */
    2,		/* timetype: 1 since last, 2 secOday(0-86400), else only timeout  */
    68256.,	/* time_end  time in days or secOday */
    1.1,	/* timeout / days */
    8.00,       /* 8      Tref  */
    0.,         /* Voff  */
    -200e-6,    /* Voffmin - minimum value of Voff (bottom interaction) */
    0.,         /* Moff */
    5.8e-6,      /* 10.e-6 Air */
    3.64e-6,    /* 3.864e-6 Compress */
    0.724e-4,    /* 0.741e-4 Thermal_exp */
    1025.0,    /* isopycnal goal */
    8000.,        /*2e4 iso_time seek time */
    -10.,	  /* 6 seek_Pmin*/
    250.,       /* seek_Pmax  */
    2.5,        /* 1  Gamma pseudo_Compressibility 1=isopycnal*/
    3600.,        /* time2 - used in mission programs*/
    1000.,        /* 0  closed_time sec  drogue opens after this*/
  };

static struct settle Settle = {
    4000.,     /* timeout / sec */
    4000.,	   /*seek_time /sec */
    200.,       /*      decay time for seeking after end of seek_time */
    20,           /* nav - (20) number of points to average to get volume */
    100.,        /* 100 drogue_time /sec - drogue open */
    6., 	/* 3 beta -  PseudoComp (big for stable) */
    300.,       /* 300  tau - seek time (big for stable)*/
    0.1,        /* 0.1  weight_error */
    0.5e-6, /* 0.5e-6 Vol_error */
    1,            /* nskip  0 for no settle*/
    1, 		/* driftset 1 - goal= Ballast.rho0, 0- from Start rho0 */
    1023.5,   /* rho0  - don't need to set */
    50e-6,  /* B0 - don't need to set */
    0.,		/* Bmin */
  650.e-6   /* Bmax */
};


static struct ballast Ballast ={
    0,             /*STPset:  0 drift end, 1 good settle, 2 at Pgoal, else never*/
    3,             /* Vset  1 settle, 0 fixed, 2 Settle&Drift, 3 Drift */
    0,		/* MLsigmafilt  1 to LP filter, else Ballast.rho0 */
    -10.,       /* 4 MLthreshold  -ML mode when less than THIS*Pdev  */
    -10.,       /* 2m MLminimum depth */
   -10.,	/* MLtop */
   -10,	/* MLbottom */
    -10,	/* SEEKthreshold */
	0.,	/* Offset */
    8.0,            /* T0    First guess */
    33.3,         /* 33.3 S0  */
    0.,            /* P0   */
    1023.5,        /* rho0 */
    150.e-6,             /*B0 guess of Ballast - not crucial */
    0.0478246,       /*  V0  - Important - First guess for Settle */
    8.0,		/* TH0  */
    0., /* Vdev - don't set */
    130.      /* Pgoal - goal for settle if good */
};

static struct ctd CTD ={
BOTTOMCTD,  /* which CTDs to use (BOTTOM TOP MEAN MAX) */
10,		/* BadMax - max # of bad before error */
1.7,		/* Ptopmin - top CTD bad above this */
0.106,      /* Poffset - Pressure offset
		0.106m =(33.5"/2 -12.5)*0.025  (5/21/06)
		adjusts pressure to be that at middle of float hull */
1.42,		/* CTD separation/m  */
0.,		/* Top Sal offset */
0.,		/* Top T offset */
0.,		/* Bott Sal offset */
0.		/* Bott T offset */
};

static struct mlb Mlb={
	1, /* go  1=compute mlb density */
	0, /*record 1=yes to record data right now, 0 no */
	0, /* pointer  - 0 means empty */
	40, /*Nmin */
	2.,   /*2 dP  gridding Pressure interval*/
	0.02, /*0.02  dSig pot density search grid*/
	0.2,     /* Sigoff   final target = MLsigma + Sigoff*/
	0.,0.,0.,0.      /* Psave, Sigsave, Pgrid, Siggrid arrays */
};
struct mlb *pMlb; /* pointer at Mlb */

static struct eos EOS={    /* Not used in MLB mission */
120.,     /* first, deepest */
50.,
20.,
10.,  /* last, shallowest */
6000.,  /* Settle duration */
0          /* comm, 0-> no comm mode on each cycle */
};

static struct bugs Bugs={
	  3600.,  /* start      sunset  / seconds of GMT day 0-86400 */
	40000.,  /*stop      sunrise */
	0.1,		/* start_weight  sunset  / kg     Zero weights to turn off this code  */
	0.03,		/*stop_weight;  /* sunrise weight /kg - linear interpolation between */
	10000,	/* flap_interval:  /* time between flaps / seconds */
	130.,		/* flap_duration: time between close and open / seconds  */
	0.		/* weight - don't set  */
};

static struct steps Steps={    /* not used in MLB */
1024,   /* Sig0  - don't set - set in program */
0.2,	/* dSig1 */
0.28,	/* dSig2 */
0.32,	/* dSig3 */
4000.,  /* time0 */
15000.,   /*time1 */
15000.,   /*time2 */
15000.   /* time3 */
};

# define Nav  100         /*Size of averaging array in Settle, Settle.nav must be smaller than this*/
# define RHOMIN 900  /* minimum allowed density */
# define NLOG     100   /* output data every NLOG calls  */
# define NCOMMAND 2 /* Number of real commands  1 2 3 ... NCOMMAND */

/* MISC variables - also satellite settable */
static double Mass0 = 49.1588;    /* initial mass */
static double Creep=0.00;  /* kg/day */
static double deep_control = 25.e-6;
static double bottom_P=150.;  /* Depth to push out piston */
static double error_P=180.; /* Depth to declare emergency */
static double top_P=-100;   /* Minimum allowed depth */
static double shallow_control=3e-6;  /*  m^2/dbar */
static short int stage=0;   /* Master mode control variable */
static short int newstage=0;  /* set this by satellite to next desired stage,
for orderly change at end of present stage */
static short Command_Repeat=1; /* number of times command must be repeated to be valid 
						Less than 1 is same as 1*/

/* Butterworth filter structures */
/* Each holds both filter coeff and previous values
	so a separate structure is needed for each filter*/

/* Prototypes for holding coefficients of each type*/
static struct butter ButterLow= {  /* prototype LP filter */
   7200.,   /*  Tfilt / sec  - only specify this here */
   0.,0.,0.,0.,0.,0.,0.,0.,0.   /* program fills in these */
};

/* all of these are specified from above */
static struct butter ButterHi; /* prototype HP filter */
static struct butter FiltPlow;  /* filters for each variable */
static struct butter FiltPhi;
static struct butter FiltPdev;
static struct butter FiltSiglow;


